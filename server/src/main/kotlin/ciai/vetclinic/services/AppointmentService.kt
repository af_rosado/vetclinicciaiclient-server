package ciai.vetclinic.services

import ciai.vetclinic.models.AppointmentDAO
import ciai.vetclinic.repositories.AppointmentsRepository
import ciai.vetclinic.repositories.ScheduleRepository
import ciai.vetclinic.repositories.ShiftRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

@Service
class AppointmentService {

    @Autowired
    lateinit var  apts:AppointmentsRepository

    @Autowired
    lateinit var shifts:ShiftRepository

    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var petService: PetService

    @Autowired
    lateinit var veterinarianService: VeterinarianService

    fun getAll() = apts.findAll().toList()

    fun getAllShifts() = shifts.findAll().toList()

    fun getAllShiftsOnDate(date: LocalDateTime) = shifts.getAllOnDate(date).get().toList();

    fun getApt(id: Long): AppointmentDAO {
        return apts.findById(id).orElseThrow{NotFoundException("There is no appointment with Id $id")}
    }

    fun createAppointment(clientId: Long, petId: Long, veterinarianId: Long, date: LocalDate,
                          description: String) : AppointmentDAO {

        var client = clientService.getClient(clientId)
        var pet = petService.getPet(petId)
        var veterinarian = veterinarianService.getVet(veterinarianId)

        var appointment = AppointmentDAO(0L, date, description, pet, veterinarian, client)

        apts.save(appointment)

        return appointment
    }
}