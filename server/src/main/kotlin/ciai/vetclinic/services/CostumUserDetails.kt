package ciai.vetclinic.services

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import java.util.stream.Collectors

class CostumUserDetails: UserDetails {

    val email: String
    val pw: String
    val id: Long
    val roles: MutableList<String>

    constructor(email: String, password: String, id: Long, roles: MutableList<String>){
        this.email = email
        this.pw = password
        this.id = id
        this.roles = roles
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return this.roles.stream().map { role -> SimpleGrantedAuthority(role)}.collect(Collectors.toList())
    }

    override fun isEnabled(): Boolean {
        return true
    }

    override fun getUsername(): String {
        return this.email
    }

    override fun isCredentialsNonExpired(): Boolean {
        return true
    }

    override fun getPassword(): String {
        return this.pw
    }

    override fun isAccountNonExpired(): Boolean {
        return true
    }

    override fun isAccountNonLocked(): Boolean {
        return true
    }
}