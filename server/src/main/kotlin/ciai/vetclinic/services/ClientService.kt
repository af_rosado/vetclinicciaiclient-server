package ciai.vetclinic.services

import ciai.vetclinic.models.*
import ciai.vetclinic.repositories.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import java.util.*

@Service
class ClientService {

    @Autowired
    lateinit var users:UserRepository

    @Autowired
    lateinit var clients:ClientRepository

    @Autowired
    lateinit var pets:PetRepository

    @Autowired
    lateinit var vets:VeterinarianRepository

    @Autowired
    lateinit var apts: AppointmentsRepository

    fun getAll() = clients.findAll().toList()

    fun getAllU() = users.findAll().toList()

    fun addClient(user: User): User {
        val email = user.email

        if(!users.findByEmail(email).isPresent){
            user.password = BCryptPasswordEncoder().encode(user.password)
            users.save(user)
            return user
        }

        else throw EmailAlreadyRegisteredException("The email: $email is already registered")
    }

    fun getClient(id:Long): ClientDAO {
        return clients.findById(id).orElseThrow{NotFoundException("There is no client with Id $id")}
    }

    fun getUserById(id:Long): User{
        return users.findById(id).orElseThrow{NotFoundException("There is no client with Id $id")}
    }

        fun getUserByEmail(email: String): User {
            return users.findByEmail(email).orElseThrow { NotFoundException("There is no client with email $email") }
    }

    fun updateClient(newClient:ClientDAO, id:Long) {
        if(clients.existsById(id)) {
            val original: ClientDAO = getClient(id)

            original.updateInfo(newClient)

            clients.save(original)
        }
        else
            throw NotFoundException("There is no client with Id $id")
    }

    fun updatePicture(newClient:ClientDAO, id:Long) {
        if(clients.existsById(id)) {
            val original: ClientDAO = getClient(id)

            original.updatePicture(newClient)

            clients.save(original)
        }
        else
            throw NotFoundException("There is no client with Id $id")
    }

    fun updatePassword(newClient:ClientDAO, id:Long) {
        if(clients.existsById(id)) {
            val original: ClientDAO = getClient(id)

            original.updatePassword(newClient)

            clients.save(original)
    }
        else
            throw NotFoundException("There is no client with Id $id")
    }

    fun addPet(pet:PetDAO): PetDAO{
        if(pet.id != 0L)
            throw PreconditionFailedException("Id must be 0 in insertion")
        else
            pets.save(pet)

        return pet
    }

    fun deletePet(id:Long) {
        if(pets.existsById(id)){
            val original: PetDAO = pets.findById(id).get()

            original.freeze()

            pets.save(original)
        }
        else
            throw NotFoundException("There is no pet with Id $id")
    }

    fun getAllPets(clientId: Long): List<PetDAO> {
        if(clients.existsById(clientId))
            return try {
                clients.findByIdWithPets(clientId).get().pets
            } catch (e: NoSuchElementException) {
                listOf<PetDAO>()
            }
        else
            throw NotFoundException("There is no client with Id $clientId")

    }

    fun getApts(id: Long): List<AppointmentDAO> {
        if(clients.existsById(id))
            return clients.findById(id).get().apts
        else
            throw NotFoundException("There is no client with Id $id")
    }

    fun addApt(apt:AppointmentDAO): AppointmentDAO {
        if(!pets.existsById(apt.pet.id))
            throw NotFoundException("There is no pet with Id $apt.pet.id")
        else if(!vets.existsById(apt.vet.id))
            throw NotFoundException("There is no veterinarian with Id $apt.vet.id")
        else if(!clients.existsById(apt.client.id))
            throw NotFoundException("There is no client with Id $apt.client.id")
        else {
            apts.save(apt)
            return apt
        }
    }
}