package ciai.vetclinic.services

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

//class PetNotFoundException(s:String): RuntimeException(s)
//class ClientNotFoundException(s:String): RuntimeException(s)

class NotFoundException(s:String): Exception(s)
class PreconditionFailedException(s:String) : Exception(s)

class EmailAlreadyRegisteredException(s:String) : Exception(s)