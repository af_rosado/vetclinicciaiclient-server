package ciai.vetclinic.models

import java.time.LocalDateTime

interface Shift {
    var id: Long
    var startDate: LocalDateTime
    var endDate: LocalDateTime
    //var vetId: Long
    var duration: Int
}