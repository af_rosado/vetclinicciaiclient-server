package ciai.vetclinic.models

import ciai.vetclinic.api.AppointmentDTO
import ciai.vetclinic.api.AppointmentDescriptionDTO
import java.time.LocalDate
import java.util.Date
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class AppointmentDAO (
        @Id @GeneratedValue var id: Long,
        var date: LocalDate,
        var description: String,
        @ManyToOne
        var pet: PetDAO,
        @ManyToOne
        var vet: VeterinarianDAO,
        @ManyToOne
        var client: ClientDAO

){
    constructor(): this(0L,LocalDate.now(),"",PetDAO(),VeterinarianDAO(),ClientDAO())

    constructor(apt: AppointmentDescriptionDTO) : this(0L, LocalDate.now(), apt.desc, PetDAO(), VeterinarianDAO(), ClientDAO())

    constructor(apt: AppointmentDTO, pet: PetDAO, vet:VeterinarianDAO, client: ClientDAO) :
            this(apt.id, LocalDate.parse(apt.date), apt.description, pet, vet, client)

    fun reSchedule(date: LocalDate) {
        this.date = date
    }

    fun updateDescription(description: String) {
        this.description = description
    }
}