package ciai.vetclinic.models

import ciai.vetclinic.api.PetDTO
import javax.persistence.*

@Entity
data class PetDAO(
        @Id @GeneratedValue val id:Long,
        var name: String,
        var species: String,
        var age: Int,
        var description: String,
        var status: String,
        var picture: String,
        var medicalRecord: String,
        @ManyToOne
        var owner: ClientDAO,
        var frozen: Boolean,
        @OneToMany(mappedBy = "pet")
        var apts: List<AppointmentDAO>
) {
    constructor(): this(0L,"","",0,"","","", "", ClientDAO(), false, emptyList())

    constructor(pet:PetDTO, owner:ClientDAO, apts:List<AppointmentDAO>) :
            this(pet.id,pet.name,pet.species, pet.age, pet.description, pet.status, pet.picture, "",owner,false,apts)

    fun freeze(){
        this.frozen = true
    }

//    fun updateInfo(other:PetDAO) {
//        this.name = other.name;
//        this.age = other.age;
//        this.description = other.description;
//        this.status = other.status;
//        this.picture = other.picture;
//    }
//
//    fun updateMedicalRecord(entry: String){
//        this.medicalRecord.add(entry)
//    }
}


