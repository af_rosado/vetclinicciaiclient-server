package ciai.vetclinic.models

import ciai.vetclinic.repositories.ShiftRepository
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import java.time.*
import java.time.temporal.ChronoUnit
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList


@Entity
data class ScheduleDAO (
        @Id @GeneratedValue var id: Long,
        var month: LocalDateTime,
        var vetId: Long,
        @OneToMany
        var shifts: MutableList<ShiftDAO>
){


    constructor(): this(0L, LocalDateTime.now(),0L, ArrayList())

    constructor(vet:VeterinarianDAO): this(0L, LocalDateTime.now(), vet.id, ArrayList())

    constructor(id: Long, month: LocalDateTime) :
            this(id, month,0L ,ArrayList())

    constructor(id: Long, month: Date, vet: VeterinarianDAO) :
            this(
                    id,
                    month.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime(),
                    0L,
                    ArrayList()
            )


    fun addShifts(shifts: List<SimpleShift>, vet: VeterinarianDAO, shiftsRepo: ShiftRepository): Boolean {
        if (sumMonthHours(shifts) != -1) {
            month = shifts[0].startDate
            this.shifts = createShifts(shifts, vet, shiftsRepo)
            this.vetId = vet.id
            return true
        }
        return false
    }

    private fun createShifts(shifts: List<SimpleShift>, vet: VeterinarianDAO, shiftsRepo: ShiftRepository) : MutableList<ShiftDAO> {
        var shiftDao : ShiftDAO
        var result: MutableList<ShiftDAO> = ArrayList()

        for (s in shifts) {
            shiftDao = ShiftDAO(s.id,s.startDate,s.endDate,s.duration)
            shiftDao.applyVet(vet)
            result.add(shiftDao)
            shiftsRepo.save(shiftDao)
        }

        return result
    }

    private fun sumWeekHours(shifts: List<SimpleShift>, startDay: Int, daysLeft: Int): Int {
        var count = 0
        var i = startDay
        var duration: Int
        var hoursUntilNext: Long

        while((i < (startDay + daysLeft)) && (i < shifts.size)) {
            duration = shifts[i].duration

            if (duration > 12) throw ResponseStatusException(HttpStatus.BAD_REQUEST,"Shift is longer than 12 hours")
            else if ((duration > 6) && ((i+1) < shifts.size)) {
                hoursUntilNext = shifts[i].endDate.until(shifts[i+1].endDate, ChronoUnit.HOURS)
                if (hoursUntilNext < 8) throw ResponseStatusException(HttpStatus.BAD_REQUEST,"Shifts are too close to one another")
            }

            count += duration
            i++
        }

        if (count > 40) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Week has more than 40 work hours")
        return count
    }

    private fun sumMonthHours(shifts: List<SimpleShift>): Int {
        var count: Int = 0
        var currentDay: Int = 1
        var daysLeftInWeek: Int
        var weekHours: Int

        while((currentDay - 1) < shifts.size) {
            daysLeftInWeek = getRemainingDaysWeek(shifts[currentDay-1].endDate)
            weekHours = sumWeekHours(shifts, currentDay-1, daysLeftInWeek)

            count += weekHours
            currentDay += daysLeftInWeek
        }

        if (count >= 160) return count
        else throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedules has less than 160 work hours ($count)")
    }

    private fun getRemainingDaysWeek( date:LocalDateTime): Int {
        // One week starts at Sunday and ends at Saturday (7 days)
        // WeekDay
        var dw: DayOfWeek = date.dayOfWeek
        // total number of days in the current month
        var monthDays: Int = date.toLocalDate().lengthOfMonth()
        //days left in the current month including self
        var daysLeft: Int = (monthDays - date.dayOfMonth) + 1

        if (daysLeft <= 7) return daysLeft

        return when(dw){
            DayOfWeek.SUNDAY -> 7
            DayOfWeek.MONDAY -> 6
            DayOfWeek.TUESDAY -> 5
            DayOfWeek.WEDNESDAY -> 4
            DayOfWeek.THURSDAY -> 3
            DayOfWeek.FRIDAY -> 2
            DayOfWeek.SATURDAY -> 1
        }
    }
}