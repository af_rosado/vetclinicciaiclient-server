package ciai.vetclinic.models

import ciai.vetclinic.api.ShiftDTO
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class ShiftDAO (
        @Id @GeneratedValue
        override var id: Long,
        override var startDate: LocalDateTime,
        override var endDate: LocalDateTime,
        override var duration: Int,
        @ManyToOne
        var vet: VeterinarianDAO
)
    :Shift {
    constructor(): this(0L, LocalDateTime.now(), LocalDateTime.now(),  1, VeterinarianDAO())

    constructor(start: String, end: String, duration: Int):
            this(0L,LocalDateTime.parse(start),LocalDateTime.parse(end), duration, VeterinarianDAO())

    constructor(id: Long,start: LocalDateTime,end: LocalDateTime,duration: Int):
            this(id,start, end, duration, VeterinarianDAO())

    constructor(id: Long, startDate: Date, endDate: Date, duration: Int):
            this(
                    id,
                    startDate.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime(),
                    endDate.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime(),
                    duration
            )

    fun applyVet(vet: VeterinarianDAO) {
        this.vet = vet
    }
}