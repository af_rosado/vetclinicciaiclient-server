package ciai.vetclinic.repositories

import ciai.vetclinic.models.PetDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface PetRepository: CrudRepository<PetDAO,Long> {

    @Query("select p from PetDAO p where p.frozen = false ")
    fun findByNotFrozen(): List<PetDAO>
}