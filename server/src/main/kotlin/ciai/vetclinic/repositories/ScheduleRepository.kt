package ciai.vetclinic.repositories
import ciai.vetclinic.models.ScheduleDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository

interface ScheduleRepository : CrudRepository<ScheduleDAO,Long> {

    @Query("select s from ScheduleDAO s where s.vetId = id")
    fun findByVetId(id: Long): ScheduleDAO
}