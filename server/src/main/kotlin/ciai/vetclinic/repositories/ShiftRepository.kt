package ciai.vetclinic.repositories

import ciai.vetclinic.models.ShiftDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.time.LocalDateTime
import java.util.*

interface ShiftRepository : CrudRepository<ShiftDAO, Long> {

    @Query("select s from ShiftDAO s where s.startDate = :date")
    fun getAllOnDate(date: LocalDateTime    ): Optional<List<ShiftDAO>>
}