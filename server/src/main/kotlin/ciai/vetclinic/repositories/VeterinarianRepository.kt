package ciai.vetclinic.repositories

import ciai.vetclinic.models.VeterinarianDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface VeterinarianRepository: CrudRepository<VeterinarianDAO,Long> {

    @Query("select v from VeterinarianDAO  v where v.frozen = false")
    fun findByNotFrozen(): List<VeterinarianDAO>

    fun findByEmail(email: String): Optional<VeterinarianDAO>
}