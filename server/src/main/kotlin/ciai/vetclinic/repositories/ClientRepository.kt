package ciai.vetclinic.repositories

import ciai.vetclinic.models.ClientDAO
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import java.util.*

interface ClientRepository: CrudRepository<ClientDAO,Long> {

    @Query("select c from ClientDAO c left join fetch c.pets p where p.frozen = false  AND c.id = :id")
    fun findByIdWithPets(id: Long): Optional<ClientDAO>

    fun findByEmail(email: String): Optional<ClientDAO>
}