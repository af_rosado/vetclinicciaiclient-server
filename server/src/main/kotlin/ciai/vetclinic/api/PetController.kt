package ciai.vetclinic.api

import ciai.vetclinic.models.PetDAO
import ciai.vetclinic.services.CostumUserDetails
import ciai.vetclinic.services.PetService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Pet API", description = "Management operations of Pets in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/pets")
class PetController(val pets: PetService) {

    @ApiOperation(value = "View a list of registered pets", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all pets in the system"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("")
    fun getAllPets(): List<PetDTO> {
        val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

        if (!(userDetails.roles.contains("ROLE_ADMIN") || userDetails.roles.contains("ROLE_VET")))
            throw HTTPForbiddenException("Forbidden Resource")

       return pets.getAllPets().map { PetDTO(it) }
    }
//    @ApiOperation(value = "Add a new pet", response = Unit::class)
//    @ApiResponses(value = [
//        ApiResponse(code = 200, message = "Successfully added a pet"),
//        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
//        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
//    ])
//    @PostMapping("")
//    fun addPet(@RequestBody pet: PetDAO) = pets.addNewPet(PetDAO(pet))

    @ApiOperation(value = "Get the details of a single pet by id", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved pet details"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}")
    fun getPet(@PathVariable id: Long) =
            handle4xx{
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val ownerId = pets.getPet(id).owner.id

                if(userDetails.id != ownerId && userDetails.roles.contains("ROLE_CLIENT"))
                    throw HTTPForbiddenException("Forbidden Resource")
                else if(userDetails.id != ownerId && !userDetails.roles.contains("ROLE_VET"))
                    throw HTTPForbiddenException("Forbidden Resource")
                else if(userDetails.id != ownerId && !userDetails.roles.contains("ROLE_ADMIN"))
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    pets.getPet(id).let {PetDTO(it)}
            }

//    @ApiOperation(value = "Update a pet", response = Unit::class)
//    @ApiResponses(value = [
//        ApiResponse(code = 200, message = "Successfully updated a pet"),
//        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
//        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
//    ])
//    @PutMapping("/{id}")
//    fun updatePet(@RequestBody pet: PetDAO, @PathVariable id: Long) =
//            handle4xx { pets.updatePet(pet,id) }

//    @ApiOperation(value = "Delete a pet", response = Unit::class)
//    @ApiResponses(value = [
//        ApiResponse(code = 200, message = "Successfully deleted a pet"),
//        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
//        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
//    ])
//    @PutMapping("/{id}")
//    fun deletePet(@PathVariable id: Long) =
//            handle4xx { pets.deletePet(id) }

}