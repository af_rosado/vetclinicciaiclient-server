package ciai.vetclinic.api

import ciai.vetclinic.models.*
import java.time.LocalDate
import java.time.LocalDateTime

data class PetDTO(val id:Long, val name:String, val species:String, val age:Int, val description:String, val status:String, val picture: String) {
    constructor(pet: PetDAO) : this(pet.id,pet.name,pet.species,pet.age,pet.description,pet.status,pet.picture)
}

data class UserDTO(val id: Long, val name: String, val email: String, val password: String, val phone: String, val address: String, val photo: String) {
    constructor(user: User): this(user.id, user.name, user.email, user.password, user.phone, user.address, user.picture)
}

data class ClientDTO(val id:Long, val name:String, val email:String, val password:String, val phone:String, val address:String, val picture:String) {
    constructor(client: ClientDAO) : this(client.id,client.name,client.email,client.password,client.phone,client.address,client.picture)
}

data class LoginDTO(val email: String, val password: String) {
    constructor(login: LoginDTO): this(login.email, login.password)
}

data class UserDetailsDTO(val id:Long, val name:String, val email:String, val phone:String, val address:String, val picture:String, val type: String) {
    constructor(user: User): this(user.id, user.name, user.email, user.phone, user.address, user.picture, user.type)
}

data class ClientDetailsDTO(val id:Long, val name:String, val email:String, val phone:String, val address:String, val picture:String) {
    constructor(client: ClientDTO): this(client.id,client.name,client.email,client.phone,client.address,client.picture)
}

data class ClientPasswordDTO(val password: String)

data class ClientPictureDTO(val picture: String)

data class ClientLoginDTO(val email: String, val password: String)

data class AppointmentDTO(val id:Long, val date:String, val description:String) {
    constructor(apt: AppointmentDAO): this(apt.id,apt.date.toString(),apt.description)
}
data class AppoinmentCompleteDTO(val id: Long, val date: LocalDate, val description: String, val petId: Long, val petName: String, val vetId: Long,val vetName: String, val clientId: Long, val clientName: String) {
    constructor(apt: AppointmentDAO): this( apt.id, apt.date, apt.description, apt.pet.id, apt.pet.name, apt.vet.id, apt.vet.name, apt.client.id, apt.client.name)
}

data class VeterinarianDTO(val id:Long, val name:String, val email:String, val password: String,  val phone:String, val address:String, val picture:String) {
    constructor(client: VeterinarianDAO) : this(client.id,client.name,client.email, client.password,client.phone,client.address,client.picture)
}

data class VeterinarianDetailsDTO(val id:Long, val name:String, val email:String, val phone:String, val address:String, val picture:String) {
    constructor(vet:VeterinarianDTO): this(vet.id,vet.name,vet.email,vet.phone,vet.address,vet.picture)
}

data class VeterinarianLoginDTO(val email: String, val password: String)

data class AdminDTO(val id:Long, val name:String, val email:String, val password:String, val phone:String, val address:String, val picture:String) {
    constructor(client: AdminDAO) : this(client.id,client.name,client.email,client.password,client.phone,client.address,client.picture)
}

data class AdminDetailsDTO(val id:Long, val name:String, val email:String, val phone:String, val address:String, val picture:String) {
    constructor(vet:AdminDTO): this(vet.id,vet.name,vet.email,vet.phone,vet.address,vet.picture)
}

data class AdminLoginDTO(val email: String, val password: String)

data class ShiftDTO(val id: Long, val startDate: LocalDateTime, val endDate: LocalDateTime, val vet: VeterinarianDAO, val duration: Int) {
    constructor(shift:ShiftDAO) : this(shift.id, shift.startDate, shift.endDate, shift.vet, shift.duration)
}

data class ShiftDTODetails(val id: Long, val startDate: LocalDateTime, val endDate: LocalDateTime, val vetId: Long, val duration: Int) {
    constructor(shift:ShiftDAO) : this(shift.id, shift.startDate, shift.endDate, shift.vet.id, shift.duration)
}

data class ScheduleDTO(val id: Long, val month: LocalDateTime, val vetId: Long, val shifts: List<ShiftDAO>) {
    constructor(schedule: ScheduleDAO): this(schedule.id,schedule.month,schedule.vetId,schedule.shifts)
}

data class AppointmentDescriptionDTO(val desc: String)

//data class ClientPetsDTO(val client:ClientDTO, val pets:List<PetDTO>)