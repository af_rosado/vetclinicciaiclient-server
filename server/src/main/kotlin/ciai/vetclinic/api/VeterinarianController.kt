package ciai.vetclinic.api

import ciai.vetclinic.models.AppointmentDAO
import ciai.vetclinic.models.ScheduleDAO
import ciai.vetclinic.services.CostumUserDetails
import ciai.vetclinic.services.VeterinarianService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*

@Api(value = "VetClinic Management System - Veterinarian API", description = "Management operations of Veterinarians in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/vets")
class VeterinarianController {

    @Autowired
    lateinit var vets: VeterinarianService

    @ApiOperation(value = "Returns all veterinarians in the system", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all vets"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("")
    fun getAllVets() =
        vets.getAll().map{VeterinarianDetailsDTO(VeterinarianDTO(it))}

    @ApiOperation(value = "Get the details of a veterinarian by id", response = VeterinarianDetailsDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved client details"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}")
    fun getVet(@PathVariable id: Long): VeterinarianDetailsDTO =
            handle4xx{vets.getVet(id)
                    .let{ VeterinarianDetailsDTO(VeterinarianDTO(it)) }}

    @ApiOperation(value = "Change the info of an appointment", response = AppointmentDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully changed appointment info"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/apt/{id}/change")
    fun changeInfo(@PathVariable id:Long,@RequestBody apt: AppointmentDescriptionDTO){
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

            if(!userDetails.roles.contains("ROLE_VET"))
                throw HTTPForbiddenException("Forbidden Resource")

            vets.changeAptInfo(id,AppointmentDAO(apt))
        }
    }

    @ApiOperation(value = "Returns the all the appointments of a veterinarian", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved th vet's appointments"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}/apts")
    fun getVetApts(@PathVariable id: Long) =
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

            if(!(userDetails.roles.contains("ROLE_ADMIN") || userDetails.roles.contains("ROLE_VET")))
                throw HTTPForbiddenException("Forbidden Resource")

            vets.getApts(id).map{AppoinmentCompleteDTO(it)}
        }

    @ApiOperation(value = "Returns the schedule of a vet", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the vet's schedule"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}/schedule")
    fun getVetSchedule(@PathVariable id: Long): ScheduleDTO {
        return ScheduleDTO(vets.getSchedule(id))
    }
}