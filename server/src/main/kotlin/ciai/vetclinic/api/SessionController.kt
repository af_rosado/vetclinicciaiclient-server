package ciai.vetclinic.api

import ciai.vetclinic.models.*
import ciai.vetclinic.services.AdminService
import ciai.vetclinic.services.ClientService
import ciai.vetclinic.services.VeterinarianService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@Api(value = "VetClinic Management System - Session API", description = "Managemento of session operations in the IADI 2019 Pet Clinic")
@RestController
class SessionController {

    @Autowired
    lateinit var clients: ClientService

    @Autowired
    lateinit var admins: AdminService

    @ApiOperation(value="Logs a user in", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully logged in"),
        ApiResponse(code = 400, message = "Unauthorized, invalid credentials")
    ])
    @PostMapping("/login")
    fun login(@RequestBody login: LoginDTO){
    }

    @ApiOperation(value="Registers an administrator in the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered an administrator"),
        ApiResponse(code = 403, message = "Forbidden operation"),
        ApiResponse(code = 500, message = "User already exists")
    ])
    @PostMapping("/register/admin")
    fun registerAdmin(@RequestBody admin: AdminDTO){
        admins.addAdmin(AdminDAO(admin))
    }

    @ApiOperation(value="Registers a veterinarian in the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered a veterinarian"),
        ApiResponse(code = 403, message = "Forbidden operation")
    ])
    @PostMapping("/register/vet")
    fun registerVet(@RequestBody vet: VeterinarianDTO){
        admins.addVet(VeterinarianDAO(vet))
    }

    @ApiOperation(value="Registers a client in the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered a client"),
        ApiResponse(code = 403, message = "Forbidden operation")
    ])
    @PostMapping("/register/client")
    fun registerClient(@RequestBody client: ClientDTO){
        clients.addClient(ClientDAO(client, emptyList(), emptyList()))
    }

    @ApiOperation(value="Registers the first admin in the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully registered the first admin"),
        ApiResponse(code = 403, message = "Forbidden operation")
    ])
    @PostMapping("/register/firstadmin")
    fun registerFirstAdmin(): AdminDetailsDTO {
        val admin = admins.addFirstAdmin()

        return AdminDetailsDTO(AdminDTO(admin))
    }
}