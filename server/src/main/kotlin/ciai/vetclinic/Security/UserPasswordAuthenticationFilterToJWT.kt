package ciai.vetclinic.Security

import ciai.vetclinic.models.UserDAO
import ciai.vetclinic.services.ClientService
import ciai.vetclinic.services.CostumUserDetailsService
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.HashMap
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.springframework.beans.factory.annotation.Autowired

class UserPasswordAuthenticationFilterToJWT(
        defaultFilterProcessesUrl: String?,
        private val anAuthenticationManager: AuthenticationManager,
        val costumUserDetails: CostumUserDetailsService,
        var clients: ClientService
): AbstractAuthenticationProcessingFilter(defaultFilterProcessesUrl) {

    object JWTSecret {
        private const val passphrase = "andrepedrovascociai"
        val KEY: String = Base64.getEncoder().encodeToString(passphrase.toByteArray())
        const val SUBJECT = "JSON Web Token for VetClinic"
        const val VALIDITY = 1000 * 60 * 60 * 10
    }

    private fun addResponseToken(authentication: Authentication, response: HttpServletResponse) {
        val claims = HashMap<String, Any?>()

        val user = clients.getUserByEmail(authentication.name)

        claims["username"] = authentication.name
        claims["id"] = user.id
        claims["type"] = user.type

        val token = Jwts
                .builder()
                .setClaims(claims)
                .setSubject(JWTSecret.SUBJECT)
                .setIssuedAt(Date(System.currentTimeMillis()))
                .setExpiration(Date(System.currentTimeMillis() + JWTSecret.VALIDITY))
                .signWith(SignatureAlgorithm.HS256, JWTSecret.KEY)
                .compact()

        response.addHeader("Authorization", "Bearer $token")
    }

    override fun attemptAuthentication(request: HttpServletRequest?,
                                       response: HttpServletResponse?): Authentication? {
        val user = ObjectMapper().readValue(request!!.inputStream, UserDAO::class.java)

        val auth = anAuthenticationManager.authenticate(UsernamePasswordAuthenticationToken(user.email,user.password))

        return if(auth.isAuthenticated) {
            SecurityContextHolder.getContext().authentication = auth
            auth
        }
        else
            null
    }

    override fun successfulAuthentication(request: HttpServletRequest,
                                          response: HttpServletResponse,
                                          filterChain: FilterChain?,
                                          auth: Authentication) {
        addResponseToken(auth,response)
    }
}