package ciai.vetclinic.Security

import ciai.vetclinic.services.ClientService
import ciai.vetclinic.services.CostumUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
class SecurityConfiguration(@field:Autowired val costumUserDetails: CostumUserDetailsService, val clients: ClientService)
    :WebSecurityConfigurerAdapter() {

    override fun configure(auth: AuthenticationManagerBuilder?) {
        auth?.let{
            it.authenticationProvider(getAuthProvider())
        }
    }

    override fun configure(http: HttpSecurity?) {
        http?.let{
            it.cors().and().csrf().disable()
        }

        http?.let {
            it.authorizeRequests()
                    //.antMatchers(HttpMethod.POST, "/login").permitAll()
                    //.antMatchers("/**").permitAll()
                    .antMatchers(HttpMethod.POST, "/login").permitAll()
                    .antMatchers(HttpMethod.POST, "/register/admin").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/register/vet").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/register/client").permitAll()
                    .antMatchers(HttpMethod.GET,"/admins").permitAll()
                    .antMatchers(HttpMethod.GET,"/admins/{\\d+}").permitAll()
                    //.antMatchers(HttpMethod.POST,"/admins").hasRole("ADMIN")
                    .antMatchers(HttpMethod.DELETE,"/admins/{\\d+").hasRole("ADMIN")
                    .antMatchers("/admins/vets/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/vets").permitAll()
                    .antMatchers(HttpMethod.GET,"/vets/{/\\d+}").permitAll()
                    .antMatchers("/vets/{\\d+}/apts").hasAnyRole("ADMIN", "VET")
                    .antMatchers("/vets/apt/{\\d+}/change").hasRole("VET")
                    .antMatchers("/vets/{\\d+}/apts").hasAnyRole("ADMIN", "VET")
                    .antMatchers("vets/{\\d+}/schedule").permitAll()
                    .antMatchers(HttpMethod.GET, "/clients").hasAnyRole("ADMIN", "VET")
                    .antMatchers("/clients/{\\d+}").authenticated()
                    .antMatchers(HttpMethod.GET,"/apts").hasAnyRole("ADMIN", "VET")
                    .antMatchers(HttpMethod.GET,"/apts/**").permitAll()
                    .antMatchers(HttpMethod.GET, "/apts/{\\d+}").hasAnyRole("ADMIN", "VET")
                    .antMatchers(HttpMethod.GET, "/pets").authenticated()
                    .antMatchers(HttpMethod.GET, "/pets/{\\d+}").hasAnyRole("ADMIN", "VET")
                    .antMatchers(HttpMethod.GET, "/clients/{\\d+}/apts").authenticated()
                    .antMatchers(HttpMethod.POST, "/clients/{\\d+}/apts/{\\d+}/{{\\d+}").hasRole("CLIENT")
                    .antMatchers("/clients/{\\d+}/pets").authenticated()
                    .antMatchers("/clients/{\\d+}/pet").authenticated()
                    .antMatchers(HttpMethod.PUT, "/clients/{\\d+}/pet/{\\d+}").hasRole("CLIENT")
                    .and()
                    .addFilterBefore(UserPasswordAuthenticationFilterToJWT("/login", authenticationManager(),
                            costumUserDetails,clients), UsernamePasswordAuthenticationFilter::class.java)
                    .formLogin().disable()
                    .headers().frameOptions().disable()
        }
    }

    fun getAuthProvider(): DaoAuthenticationProvider?{
        val authProvider = DaoAuthenticationProvider()
        authProvider.setUserDetailsService(costumUserDetails)
        authProvider.setPasswordEncoder(BCryptPasswordEncoder())
        return authProvider
    }
}