package ciai.vetclinic

import ciai.vetclinic.repositories.UserRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class VetclinicApplication

fun main(args: Array<String>) {
    runApplication<VetclinicApplication>(*args)
}
