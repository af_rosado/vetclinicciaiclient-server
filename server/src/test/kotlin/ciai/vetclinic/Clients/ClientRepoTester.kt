package ciai.vetclinic.Clients

import ciai.vetclinic.models.ClientDAO
import ciai.vetclinic.repositories.ClientRepository
import org.hamcrest.CoreMatchers
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not

@RunWith(SpringRunner::class)
@SpringBootTest
class ClientRepoTester {

    @Autowired
    lateinit var clients: ClientRepository

    companion object Constants {
        val joe = ClientDAO(-1L,"Joe","email","picture","password","phone","address", emptyList(), emptyList())
        val chan = ClientDAO(-1L,"Chan","email","picture","password","phone","address", emptyList(), emptyList())
    }

    @Test
    fun `test on single add`() {
        val client = clients.save(joe)
        assertThat(client.id, not(equalTo(joe.id)))
        assertThat(client.name, equalTo(joe.name))
        assertThat(client.email, equalTo(joe.email))
        assertThat(client.password, equalTo(joe.password))
        assertThat(client.phone, equalTo(joe.phone))
        assertThat(client.address, equalTo(joe.address))
        assertThat(client.picture, equalTo(joe.picture))

        assertThat(clients.findAll().toList(), equalTo(listOf(client)))

        clients.delete(client)

        assertThat(clients.findAll().toList(), equalTo(emptyList()))
    }

    @Test
    fun`add two`() {
        val joe = clients.save(joe)
        val chan = clients.save(chan)

        assertThat(clients.findAll().toList().size, equalTo(2))
        assertThat(clients.findAll().toList(), equalTo(listOf(joe,chan)))
    }

}