package ciai.vetclinic.Clients

import ciai.vetclinic.api.ClientDTO
import ciai.vetclinic.api.PetDTO
import ciai.vetclinic.models.ClientDAO
import ciai.vetclinic.models.PetDAO
import ciai.vetclinic.services.ClientService
import ciai.vetclinic.services.NotFoundException
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.hamcrest.Matchers.hasSize
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.Assert.assertThat
import org.hamcrest.CoreMatchers.equalTo
import org.springframework.http.MediaType

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTester {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var clients: ClientService

    companion object {
        val mapper = ObjectMapper().registerModule(KotlinModule())

        val joe = ClientDAO(1L,"Joe","email","password","phone","address","picture", emptyList(), emptyList())
        val chan = ClientDAO(2L,"Chan","email","password","phone","address","picture", emptyList(), emptyList())
        val clientsDAO = mutableListOf(joe, chan)

        val clientsDTO = clientsDAO.map{ ClientDTO(it.id,it.name,it.email,it.password,it.phone,it.address,it.picture)}

        val clientsURL = "/clients"
    }

    fun <T>nonNullAny(t:Class<T>):T = Mockito.any(t)

    @Test
    fun` Test GET all clients`() {
        Mockito.`when`(clients.getAll()).thenReturn(clientsDAO)

        val result = mvc.perform(get(clientsURL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize<Any>(clientsDTO.size)))
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<List<ClientDTO>>(responseString)
        assertThat(responseDTO, equalTo(clientsDTO))
    }

    @Test
    fun`Test get on client`() {
        Mockito.`when`(clients.getClient(1L)).thenReturn(joe)

        val result = mvc.perform(get("$clientsURL/1"))
                .andExpect(status().isOk)
                .andReturn()

        val responseString = result.response.contentAsString
        val responseDTO = mapper.readValue<ClientDTO>(responseString)
        assertThat(responseDTO, equalTo(clientsDTO[0]))
    }

    @Test
    fun`Test GET one client (Not Found)`() {
        Mockito.`when`(clients.getClient(2L)).thenThrow(NotFoundException("Client not found"))

        mvc.perform(get("$clientsURL/2"))
                .andExpect(status().is4xxClientError)
    }

    @Test
    fun `Test POST One client`() {
        val ross = ClientDTO(0, "ross", "email", "password", "phone", "address", "picture")
        val rossDAO = ClientDAO(ross.id, ross.name, ross.email, ross.password, ross.phone, ross.address, ross.picture, emptyList(), emptyList())

        val rossJSON = mapper.writeValueAsString(ross)

        Mockito.`when`(clients.addClient(nonNullAny(ClientDAO::class.java)))
                .then { assertThat(it.getArgument(0), equalTo(rossDAO)) }

        mvc.perform(post(clientsURL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(rossJSON))
                .andExpect(status().isOk)
    }

    @Test
    fun`Test Put add a pet`() {
        val pantufas = PetDTO(0L, "pantufas", "Dog", 2, "not a cat", "fine", "dog picture")
        val pantufasDAO = PetDAO(pantufas,joe, emptyList())
        joe.pets = listOf(pantufasDAO)

        val pantufasJSON = mapper.writeValueAsString(pantufas)

        Mockito.`when`(clients.addPet((nonNullAny(PetDAO::class.java))))
                .then{ assertThat(it.getArgument(0), equalTo(pantufasDAO)); it.getArgument(0)}

        Mockito.`when`(clients.getClient(1)).thenReturn(joe)

        mvc.perform(put("$clientsURL/1/pet")
                .contentType(MediaType.APPLICATION_JSON)
                .content(pantufasJSON))
                .andExpect(status().isOk)
    }

//    @Test
//    fun`Test PUT update details`() {
//        val newJoe = ClientDTO(1L,"newjoe","newemail","newpassword","newphone","newaddress","newpicture")
//        val newJoeDAO = ClientDAO(newJoe.id, newJoe.name, newJoe.email, newJoe.password, newJoe.phone, newJoe.address, newJoe.picture, emptyList())
//
//        val newJoeJSON = mapper.writeValueAsString(newJoe)
//
//        Mockito.`when`(clients.updateClient(Mockito.any(ClientDAO::class.java), joe.id))
//                .then{
//                    val client: ClientDAO = it.getArgument(0)
//                    assertThat(client.id, equalTo(joe.id))
//                    assertThat(client.name, equalTo(joe.name))
//                    assertThat(client.email, equalTo(joe.email))
//                    assertThat(client.password, equalTo(joe.password))
//                    assertThat(client.phone, equalTo(joe.phone))
//                    assertThat(client.address, equalTo(joe.address))
//                    assertThat(client.picture, equalTo(joe.picture))
//                    client
//                }
//
//        mvc.perform(put(clientsURL)
//                .contentType(MediaType.APPLICATION_JSON)
//                .content(newJoeJSON))
//                .andExpect(status().isOk)
//    }
}