package ciai.vetclinic.Clients

import org.hamcrest.CoreMatchers.equalTo
import org.junit.Assert.assertThat
import ciai.vetclinic.models.ClientDAO
import ciai.vetclinic.models.PetDAO
import ciai.vetclinic.repositories.ClientRepository
import ciai.vetclinic.repositories.PetRepository
import ciai.vetclinic.services.ClientService
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import java.util.*
import org.hamcrest.CoreMatchers.not

@RunWith(SpringRunner::class)
@SpringBootTest
class ClientServiceTester {

    @Autowired
    lateinit var clients: ClientService

    @MockBean
    lateinit var repo: ClientRepository

    @MockBean
    lateinit var petsRepo: PetRepository

    companion object Constants {
        val joe = ClientDAO(1L,"Joe","email","password","phone","address","picture", emptyList(), emptyList())
        val newJoe = ClientDAO(1L,"newJoe","newEmail","password","newPhone","newAddress","newPicture", emptyList(), emptyList())
        val chan = ClientDAO(2L,"Chan","email","password","phone","address","picture", emptyList(), emptyList())
        val clientsDAO = mutableListOf(joe, chan)
        val pantufas = PetDAO(0L, "pantufas", "Dog", 2, "not a cat", "fine", "dog picture", "nothing", joe, false, emptyList())
    }

    @Test
    fun`simple test on getAll`() {
        Mockito.`when`(repo.findAll()).thenReturn(clientsDAO)

        assertThat(clients.getAll(), equalTo(clientsDAO as List<ClientDAO>))
    }

    @Test
    fun `basic test on getClient`() {
        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(joe))

        assertThat(clients.getClient(1L), equalTo(joe))
    }

    @Test
    fun`test on addClient`() {
        Mockito.`when`(repo.save(Mockito.any(ClientDAO::class.java)))
                .then{
                    val client: ClientDAO = it.getArgument(0)
                    assertThat(client.id, equalTo(joe.id))
                    assertThat(client.name, equalTo(joe.name))
                    assertThat(client.email, equalTo(joe.email))
                    assertThat(client.picture, equalTo(joe.picture))
                    assertThat(client.password, equalTo(joe.password))
                    assertThat(client.phone, equalTo(joe.phone))
                    assertThat(client.address, equalTo(joe.address))
                    client
                }

        clients.addClient(joe)
    }

    @Test
    fun `test on update client info`() {
        Mockito.`when`(repo.save(Mockito.any(ClientDAO::class.java)))
                .then{
                    val client: ClientDAO = it.getArgument(0)
                    assertThat(client.id, equalTo(joe.id))
                    assertThat(client.name, equalTo(newJoe.name))
                    assertThat(client.email, equalTo(newJoe.email))
                    assertThat(client.picture, equalTo(newJoe.picture))
                    assertThat(client.password, equalTo(newJoe.password))
                    assertThat(client.phone, equalTo(newJoe.phone))
                    assertThat(client.address, equalTo(newJoe.address))
                    client
                }
        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(newJoe))

//        clients.addClient(joe)
        clients.updateClient(newJoe, joe.id)
        assertThat(clients.getClient(1L), equalTo(newJoe))
    }

    @Test
    fun`test on update password`() {
        val newPassword = "password1";

        Mockito.`when`(repo.save(Mockito.any(ClientDAO::class.java)))
                .then{
                    val client: ClientDAO = it.getArgument(0)
                    assertThat(client.id, equalTo(joe.id))
                    assertThat(client.name, equalTo(joe.name))
                    assertThat(client.email, equalTo(joe.email))
                    assertThat(client.picture, equalTo(joe.picture))
                    assertThat(client.password, equalTo(joe.password))
                    assertThat(client.password, not(equalTo("password")))
                    assertThat(client.phone, equalTo(joe.phone))
                    assertThat(client.address, equalTo(joe.address))
                    client
                }
        Mockito.`when`(repo.findById(1L)).thenReturn(Optional.of(joe))

        clients.updatePassword(chan,joe.id)
    }

    @Test
    fun`test on add pet`() {
        Mockito.`when`(repo.save(Mockito.any(ClientDAO::class.java)))
                .then {
                    val client: ClientDAO = it.getArgument(0)
                    assertThat(client.pets.toList(), equalTo(listOf(pantufas)))
                    client
                }
        clients.addPet(pantufas)
    }
}