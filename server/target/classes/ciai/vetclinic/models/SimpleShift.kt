package ciai.vetclinic.models

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class SimpleShift (
        @Id @GeneratedValue
        override var id: Long,
        override var startDate: LocalDateTime,
        override var endDate: LocalDateTime,
        override var duration: Int
)
    : Shift {
    constructor(): this(0L, LocalDateTime.now(), LocalDateTime.now(),  1)

    constructor(start: String, end: String, duration: Int):
            this(0L,LocalDateTime.parse(start),LocalDateTime.parse(end), duration)

    constructor(id: Long, startDate: Date, endDate: Date, duration: Int):
            this(
                    id,
                    startDate.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime(),
                    endDate.toInstant()
                            .atZone(ZoneId.systemDefault())
                            .toLocalDateTime(),
                    duration
            )

//    constructor(shift: List<ShiftDTO>): this(shift.id, shift. startDate, shift.endDate, shift.vetId, shift.duration)

//    constructor(id: Long, start: LocalDate, endDate: LocalDate, duration: Int):
//            this(id, start, endDate, duration)
}