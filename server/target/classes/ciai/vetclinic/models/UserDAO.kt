package ciai.vetclinic.models

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class UserDAO (
        @Id @GeneratedValue
        var id: Long,
        var name: String,
        @Column(unique=true)
        var email: String,
        var password: String,
        var phone: String,
        var address: String,
        var picture: String,
        var type: String
)