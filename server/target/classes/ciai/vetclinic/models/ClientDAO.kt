package ciai.vetclinic.models

import ciai.vetclinic.api.ClientDTO
import ciai.vetclinic.api.ClientDetailsDTO
import ciai.vetclinic.api.ClientPasswordDTO
import ciai.vetclinic.api.ClientPictureDTO
import javax.persistence.*

@Entity
class ClientDAO (
        id:Long,
        name:String,
        email:String,
        password:String,
        phone:String,
        address:String,
        picture:String,
        @OneToMany(mappedBy = "owner")
        var pets: List<PetDAO>,
        @OneToMany(mappedBy = "client")
        var apts: List<AppointmentDAO>
): User(id,name,email,password,phone,address,picture,"CLIENT") {

    constructor(): this(0L,"","","","","","", emptyList(),emptyList())

    constructor(client: ClientDTO, pets:List<PetDAO>, apts: List<AppointmentDAO>) :
            this(client.id,client.name,client.email,client.password,client.phone,client.address,client.picture,pets, emptyList())

    constructor(client: ClientDetailsDTO):
            this(client.id,client.name,client.email,"",client.phone,client.address,client.picture, emptyList(), emptyList())

    constructor(client: ClientPasswordDTO):
            this(0,"","",client.password,"","","", emptyList(), emptyList())

    constructor(client: ClientPictureDTO):
            this(0,"","","","","",client.picture, emptyList(), emptyList())
}