package ciai.vetclinic.models

import ciai.vetclinic.api.AdminDTO
import ciai.vetclinic.api.LoginDTO
import ciai.vetclinic.api.UserDTO
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class AdminDAO (
        id: Long,
        name: String,
        email: String,
        password: String,
        phone: String,
        address: String,
        picture: String
): User(id,name,email,password,phone,address,picture,"ADMIN") {
    constructor(): this(0L,"","","","","","")

    constructor(admin: AdminDTO) :
            this(admin.id, admin.name, admin.email, admin.password, admin.phone, admin.address, admin.picture)
    constructor(user: UserDTO):
            this(user.id,user.name,user.email,user.password,user.phone,user.address,user.photo)
    constructor(login: LoginDTO):
            this(0, "", login.email, login.password,"","","")

    fun updateInfo(other:AdminDAO) {
        this.name = other.name
        this.email = other.email
        this.phone = other.phone
        this.address = other.address
        this.picture = other.picture
    }

    fun updatePassword(newPassword:String) {
        this.password = newPassword
    }

}