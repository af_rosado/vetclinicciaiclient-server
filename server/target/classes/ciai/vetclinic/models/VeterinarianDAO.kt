package ciai.vetclinic.models

import ciai.vetclinic.api.VeterinarianDTO
import ciai.vetclinic.api.VeterinarianDetailsDTO
import java.util.UUID.randomUUID
import javax.persistence.*

@Entity
class VeterinarianDAO (
        id: Long,
        name: String,
        email: String,
        password: String,
        phone: String,
        address: String,
        picture: String,
        var frozen: Boolean,
        @OneToMany(mappedBy = "vet")
        var apts: List<AppointmentDAO>,
        var scheduleId: Long
): User(id,name,email,password,phone,address,picture,"VET"){
    constructor(): this(0L,"","","","","","",false, emptyList(), 0L)

    constructor(vet: VeterinarianDTO):
            this(vet.id,vet.name,vet.email,randomUUID().toString(),vet.phone,vet.address,vet.picture,false, emptyList(), 0L)

    constructor(vet: VeterinarianDetailsDTO) :
            this(vet.id,vet.name,vet.email,"",vet.phone,vet.address,vet.picture, false, emptyList(), 0L)

    fun updateInfo(other:VeterinarianDAO) {
        this.name = other.name
        @Column(unique=true)
        this.email = other.email
        this.phone = other.phone
        this.address = other.address
        this.picture = other.picture
    }

    fun updatePassword(newPassword:String) {
        this.password = newPassword
    }

    fun updateSchedule(scheduleId: Long){
        this.scheduleId = scheduleId
    }

    fun freeze() {
        this.frozen = true
    }
}