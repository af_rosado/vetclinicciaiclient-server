package ciai.vetclinic.models

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
abstract class User(
        @Id @GeneratedValue val id: Long,
        var name: String,
        var email: String,
        var password: String,
        var phone: String,
        var address: String,
        var picture: String = "",
        var type: String
    ){
    constructor(): this(0, "", "", "", "", "", "" , "")

    fun updateInfo(other:ClientDAO) {
        this.name = other.name
        this.email = other.email
        this.phone = other.phone
        this.address = other.address
    }

    fun updatePicture(other:ClientDAO) {
        this.picture = other.picture
    }

    fun updatePassword(other:ClientDAO) {
        this.password = BCryptPasswordEncoder().encode(other.password)
    }
}