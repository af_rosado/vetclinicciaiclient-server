package ciai.vetclinic.services


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class CostumUserDetailsService: UserDetailsService {
    @Autowired
    lateinit var users: ClientService

    override fun loadUserByUsername(email: String?): UserDetails {
        email?.let {
            val user = users.getUserByEmail(it)
            return CostumUserDetails(user.email, user.password, user.id, mutableListOf("ROLE_" + user.type))
        }
        throw UsernameNotFoundException(email)
    }
}