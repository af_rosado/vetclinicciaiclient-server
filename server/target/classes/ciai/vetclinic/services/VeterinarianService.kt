package ciai.vetclinic.services

import ciai.vetclinic.models.AppointmentDAO
import ciai.vetclinic.models.ScheduleDAO
import ciai.vetclinic.models.VeterinarianDAO
import ciai.vetclinic.repositories.AppointmentsRepository
import ciai.vetclinic.repositories.ScheduleRepository
import ciai.vetclinic.repositories.VeterinarianRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class VeterinarianService {

    @Autowired
    lateinit var vets: VeterinarianRepository
    @Autowired
    lateinit var schedules: ScheduleRepository

    @Autowired
    lateinit var apts: AppointmentsRepository

    fun getAll() = vets.findByNotFrozen()

    fun getVet(id:Long): VeterinarianDAO {
        return vets.findById(id).orElseThrow{NotFoundException("There is no vet with Id $id")}
    }

    fun getVetByEmail(email: String): Optional<VeterinarianDAO> {
        if(vets.findByEmail(email).isPresent)
            return vets.findByEmail(email)
        else
            throw NotFoundException("There is no veterinarian with email $email")
    }

    fun changeAptInfo(id:Long,apt:AppointmentDAO) {
        if(!apts.existsById(id))
            throw NotFoundException("There is no appointment with Id $id")

        else{
            val original = apts.findById(id).get()

            original.updateDescription(apt.description)

            apts.save(original)
        }

    }

    fun getApts(id: Long): List<AppointmentDAO> {
        if(vets.existsById(id)) {
            val vet = vets.findById(id).get()

            return vet.apts
        }
        else
            throw NotFoundException("There is no veterinarian wit Id $id")
    }

    fun getSchedule(id: Long): ScheduleDAO {
        var vet = vets.findById(id).orElseThrow{NotFoundException("There is no vet with Id $id")}
        var scheduleId = vet.scheduleId

        return schedules.findById(scheduleId).orElseThrow{(NotFoundException("There is no schedule with Id $scheduleId"))}
    }

}