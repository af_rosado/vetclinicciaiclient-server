package ciai.vetclinic.services

import ciai.vetclinic.models.PetDAO
import ciai.vetclinic.repositories.PetRepository
import org.springframework.stereotype.Service

@Service
class PetService(val pets: PetRepository) {

    fun getAllPets():List<PetDAO> = pets.findByNotFrozen()

//    fun addNewPet(pet:PetDAO) {
//        pets.save(pet)
//    }
//
    fun getPet(id:Long): PetDAO{
        return pets.findById(id).orElseThrow {NotFoundException("There is no Pet with Id $id") }
}
//
//    fun updatePet(newPet:PetDAO, id:Long) {
//        val original:PetDAO = getPet(id)
//
//        original.updateInfo(newPet)
//
//        pets.save(original)
//    }
//
//    fun deletePet(id:Long) {
//        val pet:PetDAO = getPet(id)
//
//        pets.delete(pet)
//    }
}