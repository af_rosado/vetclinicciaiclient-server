package ciai.vetclinic.services

import ciai.vetclinic.models.AppointmentDAO
import ciai.vetclinic.repositories.AppointmentsRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class AppointmentService {

    @Autowired
    lateinit var  apts:AppointmentsRepository

    @Autowired
    lateinit var clientService: ClientService

    @Autowired
    lateinit var petService: PetService

    @Autowired
    lateinit var veterinarianService: VeterinarianService

    fun getAll() = apts.findAll().toList()

    fun getApt(id: Long): AppointmentDAO {
        return apts.findById(id).orElseThrow{NotFoundException("There is no appointment with Id $id")}
    }

    fun createAppointment(clientId: Long, petId: Long, veterinarianId: Long, date: LocalDate,
                          description: String) : AppointmentDAO {

        var client = clientService.getClient(clientId)
        var pet = petService.getPet(petId)
        var veterinarian = veterinarianService.getVet(veterinarianId)

        var appointment = AppointmentDAO(0L, date, description, pet, veterinarian, client)

        apts.save(appointment)

        return appointment
    }
}