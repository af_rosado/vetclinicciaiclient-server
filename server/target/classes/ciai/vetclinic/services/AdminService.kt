package ciai.vetclinic.services

import ciai.vetclinic.api.HTTPConflictException
import ciai.vetclinic.api.HTTPForbiddenException
import ciai.vetclinic.api.UserDTO
import ciai.vetclinic.api.handle4xx
import ciai.vetclinic.models.*
import ciai.vetclinic.repositories.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException
import java.util.*

@Service
class AdminService {

    @Autowired
    lateinit var users:UserRepository

    @Autowired
    lateinit var admins: AdminRepository

    @Autowired
    lateinit var vets: VeterinarianRepository

    @Autowired
    lateinit var schedules: ScheduleRepository

    @Autowired
    lateinit var shiftsRepo: ShiftRepository

    fun getAll() = admins.findAll().toList()

    fun getAllVets() = vets.findByNotFrozen()

    fun getAdmin(id: Long): AdminDAO {
        return admins.findById(id).orElseThrow{NotFoundException("There is no administrator with Id $id")}
    }

    fun getAdminByEmail(email: String): Optional<AdminDAO> {
        if(admins.findByEmail(email).isPresent)
            return admins.findByEmail(email)
        else
            throw NotFoundException("There is no administrator with email $email")
    }

    fun addAdmin(admin: AdminDAO): AdminDAO {
        val email = admin.email

        if (!users.findByEmail(email).isPresent) {
            admin.password = BCryptPasswordEncoder().encode(admin.password)
            users.save(admin)
            return admin
        }
        else throw HTTPConflictException("The email: $email is already registered")
    }

    fun removeAdmin(id:Long): List<AdminDAO> {

        if(id == 1L){
            throw HTTPForbiddenException("This admin can not be deleted")
        }
        else {
            admins.deleteById(id)
            return getAll()
        }

    }


    fun addVet(vet: VeterinarianDAO): VeterinarianDAO {
        val email = vet.email;

        if(!users.findByEmail(email).isPresent){
            vet.password = BCryptPasswordEncoder().encode(vet.password)
            users.save(vet)
            return vet
        }

        else throw EmailAlreadyRegisteredException("The email: $email is already registered")
    }

    fun removeVet(id:Long): List<VeterinarianDAO> {
        if(vets.existsById(id)) {
           val original : VeterinarianDAO = vets.findById(id).get()

            original.freeze()
            vets.save(original)

            return getAllVets()
        }
        else
            throw NotFoundException("There is no veterinarian with Id $id")
    }

    fun addSchedule(id: Long, shifts: List<SimpleShift>): ScheduleDAO {
        if(vets.existsById(id)) {
            var schedule = ScheduleDAO()
            var vet = vets.findById(id).get()

            if(schedule.addShifts(shifts, vet, shiftsRepo)){
                schedules.save(schedule)
            } else {
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Schedule not valid")
            }

            vet.updateSchedule(schedule.id)
            vets.save(vet)

            return schedule
        }
        else
            throw NotFoundException("There is no veterinarian with Id $id")
    }

    fun addFirstAdmin(): AdminDAO {
        if(!users.findByEmail("admin@mail.com").isPresent) {
            val firstAdmin = AdminDAO(0, "Michael Scott", "admin@mail.com", BCryptPasswordEncoder().encode("admin"), "987456321", "Scrantton Business Park", "https://i.imgur.com/cAHzCmB.jpg")
            users.save(firstAdmin)

            return firstAdmin
        }

        else
            throw HTTPConflictException("First admin already registred")
    }
}