package ciai.vetclinic.api

import ciai.vetclinic.services.NotFoundException
import ciai.vetclinic.services.PreconditionFailedException


fun <T> handle4xx(inner: () -> T): T =
        try {
            inner()
        } catch (e: NotFoundException) {
            throw HTTPNotFoundException(e.message ?: "Not Found")
        } catch (e : PreconditionFailedException) {
            throw HTTPBadRequestException(e.message ?: "Bad Request")
        }
        catch (e : PreconditionFailedException) {
            throw HTTPForbiddenException(e.message ?: "Forbidden")
        }