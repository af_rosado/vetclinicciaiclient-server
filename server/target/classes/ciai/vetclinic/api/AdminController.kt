package ciai.vetclinic.api

import ciai.vetclinic.models.*
import ciai.vetclinic.services.AdminService
import ciai.vetclinic.services.CostumUserDetails
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.*
import springfox.documentation.spi.service.contexts.SecurityContext

@Api(value = "VetClinic Management System - Administrator API", description = "Management operations of Administrators in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/admins")
class AdminController {

    @Autowired
    lateinit var admins: AdminService

    @ApiOperation(value = "View a list of all administrators", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all administrators in the system"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("")
    fun getAllAdmins() =
            admins.getAll().map{AdminDetailsDTO(AdminDTO(it))}

    @ApiOperation(value = "Get the details of a administrator by id", response = AdminDetailsDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved administrator details"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}")
    fun getAdmin(@PathVariable id: Long): AdminDetailsDTO =
            handle4xx{
                admins.getAdmin(id).let{ AdminDetailsDTO(AdminDTO(it)) }
            }

    @ApiOperation(value = "Add a new administrator to the system", response = AdminDetailsDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added a new administrator"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("")
    fun addAdmin(@RequestBody admin:AdminDTO): AdminDetailsDTO {
        admins.addAdmin(AdminDAO(admin))

        return AdminDetailsDTO(admin)
    }

    @ApiOperation(value = "Deletes an administrator from the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully removed the administrator"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @DeleteMapping("/{id}")
    fun removeAdmin(@PathVariable id:Long) =
            handle4xx{
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                if(!userDetails.roles.contains("ROLE_ADMIN"))
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    admins.removeAdmin(id)
            }

    @ApiOperation(value = "Adds a veterinarian to the system", response = VeterinarianDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added a new veterinarian"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/vets")
    fun addVet(@RequestBody vet:VeterinarianDTO): VeterinarianDetailsDTO {
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

            if(!userDetails.roles.contains("ROLE_ADMIN"))
                throw HTTPForbiddenException("Forbidden Resource")
            else
                admins.addVet(VeterinarianDAO(vet))
        }

        return VeterinarianDetailsDTO(vet)
    }

    @ApiOperation(value = "Removes a veterinarian from the system", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully removed the veterinarian"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/vets/{id}")
    fun deleteVet(@PathVariable id:Long) {
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

            if(!userDetails.roles.contains("ROLE_ADMIN"))
                throw HTTPForbiddenException("Forbidden Resource")
            else
                admins.removeVet(id)
        }
    }

    @ApiOperation(value = "Adds a new schedule for a Veterinarian", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added a schedule to the veterinarian"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/vets/{id}/schedule")
    fun addSchedule(@PathVariable id:Long, @RequestBody shifts:List<SimpleShift>): ScheduleDTO{
        var schedule = ScheduleDAO()
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

            if(!userDetails.roles.contains("ROLE_ADMIN"))
                throw HTTPForbiddenException("Forbidden Resource")
            else
                schedule = admins.addSchedule(id,shifts)
        }
        return ScheduleDTO(schedule)
    }
}