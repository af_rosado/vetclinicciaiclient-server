package ciai.vetclinic.api

import ciai.vetclinic.services.AppointmentService
import ciai.vetclinic.services.CostumUserDetails
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@Api(value = "VetClinic Management System - Appointments API", description = "Management operations of Appointments in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/apts")
class AppointmentController {

    @Autowired
    lateinit var apts: AppointmentService

    @ApiOperation(value = "View a list all appointments", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all appointments"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("")
    fun getAllAppointments(): List<AppointmentDTO> {
        val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

        if (!(userDetails.roles.contains("ROLE_ADMIN") || userDetails.roles.contains("ROLE_VET")))
            throw HTTPForbiddenException("Forbidden Resource")

        return apts.getAll().map { AppointmentDTO(it) }
    }


    @ApiOperation(value = "View a specific appointments", response = AppointmentDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved the appointment"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}")
    fun getAppointment(@PathVariable id:Long) =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails

                if (!(userDetails.roles.contains("ROLE_ADMIN") || userDetails.roles.contains("ROLE_VET")))
                    throw HTTPForbiddenException("Forbidden Resource")

                apts.getApt(id)
            }
}