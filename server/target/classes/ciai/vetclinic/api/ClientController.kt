package ciai.vetclinic.api

import ciai.vetclinic.models.AppointmentDAO
import ciai.vetclinic.models.ClientDAO
import ciai.vetclinic.models.PetDAO
import ciai.vetclinic.services.*
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.Http403ForbiddenEntryPoint
import org.springframework.web.bind.annotation.*
import springfox.documentation.spi.service.contexts.SecurityContext

@Api(value = "VetClinic Management System - Client API", description = "Management operations of Clients in the IADI 2019 Pet Clinic")
@RestController
@RequestMapping("/clients")
class ClientController {

    @Autowired
    lateinit var clients: ClientService

    @Autowired
    lateinit var pets: PetService

    @Autowired
    lateinit var vets: VeterinarianService

    @Autowired
    lateinit var appointments: AppointmentService

    @ApiOperation(value = "View a list of registered clients", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all clients in the system"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("")
    fun getAllClients() =
            clients.getAll().map{ClientDetailsDTO(ClientDTO(it))}

    @GetMapping("/u")
    fun getAllClientsUser() =
            clients.getAllU().map{UserDetailsDTO(it)}

    @ApiOperation(value="Add a new client", response = ClientDetailsDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added a client"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("")
    fun addClient(@RequestBody client: ClientDTO): ClientDetailsDTO {
        clients.addClient(ClientDAO(client, emptyList(), emptyList()))

        return ClientDetailsDTO(client)
    }

    @ApiOperation(value = "Get the details of a client by id", response = ClientDetailsDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved client details"),
        ApiResponse(code = 401, message = "You are not authorized to view the resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{id}")
    fun getClient(@PathVariable id: Long): UserDetailsDTO =
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
            val user = clients.getUserById(userDetails.id)

            if(user.id == id)
                clients.getClient(id).let { UserDetailsDTO(it) }
            else if (user.id != id && userDetails.roles.contains("ROLE_ADMIN"))
                clients.getClient(id).let { UserDetailsDTO(it) }
            else
                throw HTTPForbiddenException("Forbidden Resource")
        }

    @ApiOperation(value = "Update a client's details", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully updated details"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/{id}")
    fun updateClient(@RequestBody client: ClientDetailsDTO, @PathVariable id: Long) =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val user = clients.getUserById(userDetails.id)

                if(user.id != id)
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    clients.updateClient(ClientDAO(client),id)
            }

    @ApiOperation(value = "Change a client's picture", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully changed picture"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/{id}/picture")
    fun updatePassword(@RequestBody client: ClientPictureDTO, @PathVariable id: Long) =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val user = clients.getUserById(userDetails.id)
                if(user.id != id)
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    clients.updatePicture(ClientDAO(client),id)
            }

    @ApiOperation(value = "Change a client's password", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully changed password"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/{id}/password")
    fun updatePassword(@RequestBody client: ClientPasswordDTO, @PathVariable id: Long) =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val user = clients.getUserById(userDetails.id)

                if(user.id != id)
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    clients.updatePassword(ClientDAO(client),id)
            }

    @ApiOperation(value = "Add a pet to a client", response = PetDTO::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully added pet"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/{id}/pet")
    fun addPet(@RequestBody pet:PetDTO, @PathVariable id:Long) : PetDTO{
        var ret = PetDTO(1,"", "", 1, "", "", "")
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
            val user = clients.getUserById(userDetails.id)

            if(user.id != id)
                throw HTTPForbiddenException("Forbidden Resource")

            else
                ret = PetDTO(clients.addPet(PetDAO(pet,clients.getClient(id), emptyList())))
        }
        return ret
    }

    @ApiOperation(value = "Delete a pet ", response = Unit::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully deleted pet"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PutMapping("/{clientId}/pet/{petId}")
    fun deletePet(@PathVariable clientId:Long, @PathVariable petId:Long) {
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
            val user = clients.getUserById(userDetails.id)

            if(user.id != clientId)
                throw HTTPForbiddenException("Forbidden Resource")
            else
                clients.deletePet(petId)
        }
    }

    @ApiOperation(value = "Get all pets from a client", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all pets of the client"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{clientId}/pets")
    fun getClientPets(@PathVariable clientId:Long): List<PetDTO> =
        handle4xx {
            val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
            val user = clients.getUserById(userDetails.id)

            if(user.id != clientId)
                throw HTTPForbiddenException("Forbidden Resource")
            else
                clients.getAllPets(clientId).map{PetDTO(it)}
        }

    @ApiOperation(value = "Get all client appointments", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully retrieved all appointments of the client"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @GetMapping("/{clientId}/apts")
    fun getClientApts(@PathVariable clientId:Long): List<AppointmentDTO> =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val user = clients.getUserById(userDetails.id)

                if(user.id != clientId)
                    throw HTTPForbiddenException("Forbidden Resource")
                else if(user.id != clientId && (userDetails.roles.contains("ROLE_ADMIN") || userDetails.roles.contains("ROLE_VET")))
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    clients.getApts(clientId)
                        .map{AppointmentDTO((it))}
            }

    @ApiOperation(value = "Client schedules an appointment", response = List::class)
    @ApiResponses(value = [
        ApiResponse(code = 200, message = "Successfully scheduled the appointment"),
        ApiResponse(code = 401, message = "You are not authorized to use this resource"),
        ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    ])
    @PostMapping("/{clientId}/apts/{vetId}/{petID}")
    fun addApt(@PathVariable clientId:Long,
               @RequestBody apt:AppointmentDTO,
               @PathVariable petID:Long,
               @PathVariable vetId:Long
               ) =
            handle4xx {
                val userDetails: CostumUserDetails = SecurityContextHolder.getContext().authentication.principal as CostumUserDetails
                val user = clients.getUserById(userDetails.id)

                if(user.id != clientId)
                    throw HTTPForbiddenException("Forbidden Resource")
                else
                    AppointmentDTO(clients.addApt(AppointmentDAO(apt, pets.getPet(petID), vets.getVet(vetId),clients.getClient(clientId))))
            }
}