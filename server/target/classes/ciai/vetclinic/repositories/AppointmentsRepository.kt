package ciai.vetclinic.repositories

import ciai.vetclinic.models.AppointmentDAO
import org.springframework.data.repository.CrudRepository

interface AppointmentsRepository: CrudRepository<AppointmentDAO,Long>