package ciai.vetclinic.repositories

import ciai.vetclinic.models.AdminDAO
import org.springframework.data.repository.CrudRepository
import java.util.*

interface AdminRepository: CrudRepository<AdminDAO, Long> {

    fun findByEmail(email: String): Optional<AdminDAO>
}