package ciai.vetclinic.repositories

import ciai.vetclinic.models.ShiftDAO
import org.springframework.data.repository.CrudRepository

interface ShiftRepository : CrudRepository<ShiftDAO, Long> {

}