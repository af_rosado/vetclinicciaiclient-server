package ciai.vetclinic.repositories

import ciai.vetclinic.models.User
import org.springframework.data.repository.CrudRepository
import java.util.*

interface UserRepository: CrudRepository<User, Long> {
    fun findByEmail(email: String): Optional<User>
}