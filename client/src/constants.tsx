export const colors ={
    primary: '#000441',
    greyText: 'rgb(100, 100, 100)',
    white: '#ffffff',
    lightGrey: '#D3D3D3'
}

export const userTypes = {
    ADMIN: 'ADMIN',
    VET: 'VET',
    CLIENT: 'CLIENT'
}
