import { combineReducers } from 'redux';
import SessionReducer from "./reducers/LoginReducer";
import RegisterReducer from './reducers/RegisterReducer';
import { connectRouter } from 'connected-react-router';
import UsersReducer from "./reducers/UsersReducer";
import AdminReducer from './reducers/AdminReducer';
import RegisterAdminReducer from './reducers/RegisterAdminReducer';
import AuthReducer from './reducers/AuthReducer';
import RegisterVetReducer from './reducers/RegisterVetReducer';
import CurrentUserReducer from './reducers/CurrentUserReducer';
import SetVetScheduleReducer from './reducers/SetVetScheduleReducer';

const rootReducer = (history: any) => combineReducers({
   router: connectRouter(history),
   SessionReducer: SessionReducer,
   RegisterReducer: RegisterReducer,
   UsersReducer: UsersReducer,
   AdminReducer: AdminReducer,
   RegisterAdminReducer: RegisterAdminReducer,
   AuthReducer: AuthReducer,
   RegisterVetReducer: RegisterVetReducer,
   CurrentUserReducer: CurrentUserReducer,
   SetVetScheduleReducer: SetVetScheduleReducer
})

export default rootReducer;
