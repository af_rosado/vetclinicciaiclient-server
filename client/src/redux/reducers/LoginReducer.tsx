import * as types from '../Types';

const initialState = {
   email: '',
   password: '',
   invalidLogin: false
};

const SessionReducer = (state = initialState, action: any) => {
   let newState = { ...state };

   switch (action.type) {
      case types.SET_LOGIN_FORM_EMAIL:
         newState.email = action.email;
         return newState;
      case types.SET_LOGIN_FORM_PASSWORD:
         newState.password = action.password;
         return newState;
      case types.SET_VALID_LOGIN:
         newState.invalidLogin = action.validLogin;
         return newState;
      default:
         return newState;
   }
};

export default SessionReducer;
