import { GET_CURRENT_USER, GET_CURRENT_USER_PETS, GET_CURRENT_USER_APTS, SET_TO_SHOW_ID, GET_CURRENT_VET_SCHEDULE, TOGGLE_EDITION, TOGGLE_PASSWORD_CHANGE, TOGGLE_ADD_PET, TOGGLE_REMOVE_PET, SET_PET_PROFILE, TOGGLE_ADD_APT, GET_ALL_SHIFTS } from '../Types';
import { emptyUser, emptyPet, emptyAppointment, emptySchedule } from '../../interfaces/Interfaces';

const initialState = {
    user: emptyUser,
    pets: [emptyPet],
    appointments: [emptyAppointment],
    toShowId: -1,
    schedule: [emptySchedule],
    edition: false,
    passwordChange: false,
    addPet: false,
    removePet: false,
    addApt: false,
    petProfile: emptyPet,
    shifts: []
};

const CurrentUserReducer = (state = initialState, action: any) => {
    let newState = { ...state };

    switch (action.type) {
        case GET_CURRENT_USER:
            newState.user = action.user;
            return newState;
        case GET_CURRENT_USER_PETS:
            if (action.pets.length !== 0) {
                newState.pets = action.pets
            }
            return newState;
        case GET_CURRENT_USER_APTS:
            if (action.appointments.length !== 0) {
                newState.appointments = action.appointments;
            }
            return newState;
        case SET_TO_SHOW_ID:
            newState.toShowId = action.toShowId;
            return newState;
        case GET_CURRENT_VET_SCHEDULE:
            if (action.schedule.length !== 0) {
                newState.schedule = action.schedule;
            }
            return newState
        case TOGGLE_EDITION:
            newState.edition = action.edition;
            return newState;
        case TOGGLE_PASSWORD_CHANGE:
            newState.passwordChange = action.passwordChange;
            return newState;
        case TOGGLE_ADD_PET:
            newState.addPet = action.addPet;
            return newState;
        case TOGGLE_REMOVE_PET:
            newState.removePet = action.removePet;
            return newState;
        case SET_PET_PROFILE:
            newState.petProfile = action.petProfile;
            return newState
        case TOGGLE_ADD_APT:
            newState.addApt = action.addApt;
            return newState;
        case GET_ALL_SHIFTS:
            newState.shifts = action.shifts;
        default:
            return newState;
    }
};

export default CurrentUserReducer;
