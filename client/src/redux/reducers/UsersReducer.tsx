import * as types from '../Types';

const initialState = {
    admins: [],
    vets: [],
    clients: [],
    admin: {}
};

const UsersReducer = (state = initialState, action: any) => {
    let newState = {...state};

    switch (action.type) {
        case types.GET_ALL_ADMINS:
            newState.admins = action.admins;
            return newState;
        case types.GET_ALL_VETS:
            newState.vets = action.vets;
            return newState;
        case types.GET_ALL_CLIENTS:
            newState.clients = action.clients;
            return newState;
        case types.GET_ADMIN:
            newState.admin = action.admin;
            return newState;
        default:
            return newState;
    }
}

export default UsersReducer;
