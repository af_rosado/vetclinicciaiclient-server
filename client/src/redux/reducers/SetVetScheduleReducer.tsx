import * as types from '../Types';
import moment, { Moment } from 'moment';

export interface ISetVetScheduleReducer {
   vetId: number | null,
   month: Moment,
   shifts: Array<any>
}

const initialState: ISetVetScheduleReducer = {
   vetId: null,
   month: moment('2019-12-01', 'YYYY-MM-DD'),
   shifts: Array(31).fill({ start: "07:00", end: "12:00"})
};

const SetVetScheduleReducer = (state = initialState, action: any) => {
   let newState = { ...state };

   switch (action.type) {
      case types.SET_VET_TO_SCHEDULE:
         newState.vetId = action.vetId;
         return newState;
      case types.SET_VET_SCHEDULE_MONTH:
         newState.month = action.month;
         newState.shifts = Array(action.month.daysInMonth()).fill({ start: "07:00", end: "12:00"});
         return newState;
      case types.SET_VET_SHIFT_START:
         newState.shifts[action.index] = { 'start': action.start, 'end': newState.shifts[action.index].end };
         return newState;
      case types.SET_VET_SHIFT_END:
         newState.shifts[action.index] = { 'start': newState.shifts[action.index].start, 'end': action.end };
         return newState;
      default:
         return newState;
   }
};

export default SetVetScheduleReducer;
