import * as types from '../Types';

const initialState = {
   userType: '',
   id: ''
};

const AuthReducer = (state = initialState, action: any) => {
   let newState = { ...state };

   switch (action.type) {
      case types.SET_AUTH_USER:
         newState.userType = action.userType;
         newState.id = action.id;
         return newState;
      default:
         return newState;
   }
};

export default AuthReducer;
