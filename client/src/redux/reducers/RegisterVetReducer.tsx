import * as types from '../Types';

const initialState = {
   email: '',
   password: '',
   name: '',
   phone: '',
   address: '',
   picture: ''
};

const RegisterVetReducer = (state = initialState, action: any) => {
   let newState = { ...state };

   switch (action.type) {
      case types.SET_REGISTER_VET_FORM_NAME:
         newState.name = action.name;
         return newState;
      case types.SET_REGISTER_VET_FORM_EMAIL:
         newState.email = action.email;
         return newState;
      case types.SET_REGISTER_VET_FORM_PASSWORD:
         newState.password = action.password;
         return newState;
      case types.SET_REGISTER_VET_FORM_PHONE:
         newState.phone = action.phone;
         return newState;
      case types.SET_REGISTER_VET_FORM_ADDRESS:
         newState.address = action.address;
         return newState;
      case types.SET_REGISTER_VET_FORM_PHOTO:
         newState.picture = action.picture;
         return newState;
      default:
         return newState;
   }
};

export default RegisterVetReducer;
