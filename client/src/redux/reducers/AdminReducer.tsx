import * as types from '../Types';

const initialState = {
   page: 0,
};

const AdminReducer = (state = initialState, action: any) => {
   let newState = { ...state };

   switch (action.type) {
      case types.SET_ADMIN_PAGE:
         newState.page = action.page;
         return newState;
      default:
         return newState;
   }
};

export default AdminReducer;
