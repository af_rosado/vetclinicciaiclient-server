import axios from 'axios';
import * as types from '../Types';
import { push } from 'connected-react-router';
import { userTypes } from "../../constants";

var jwtDecode = require('jwt-decode');

export function registFirstAdmin() {
   return function () {
      const url = '/register/firstadmin'

      axios({
         method: 'post',
         url
      })
         .then(response => {
         })
         .catch(error => {
         })
   }
}

export function setLoginEmail(email: string) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_LOGIN_FORM_EMAIL,
         email
      })
   }
}

export function setLoginPassword(password: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_LOGIN_FORM_PASSWORD,
         password
      })
   }
}

export function submitLogin(data: object) {
   return function (dispatch: any) {
      const url = "/login"

      axios({
         method: 'post',
         url,
         data,
         headers: {
            'Content-Type': 'application/json'
         }
      })
         .then(response => {
            const token = response.headers.authorization.split(" ")[1]
            localStorage.setItem("token", token);
            let decodedToken = jwtDecode(token);
            localStorage.setItem("id", decodedToken.id)
            localStorage.setItem("id",decodedToken.id);
            localStorage.setItem("type",decodedToken.type);
            dispatch({
               type: types.SET_AUTH_USER,
               userType: decodedToken.type,
               id: decodedToken.id
            });
            dispatch({
               type: types.SET_VALID_LOGIN,
               invalidLogin: false
            })
            dispatch(push('/profile'));
         })
         .catch(error => {
            if(error.response.status === 401)
               dispatch({
                  type: types.SET_VALID_LOGIN,
                  validLogin: true
               })
         })

   }
}

export function logout() {
   return function (dispatch: any) {
      localStorage.removeItem('token');
      localStorage.removeItem('id');
      dispatch({
         type: types.SET_AUTH_USER,
         userType: '',
         id: ''
      });
      localStorage.removeItem('id');
      localStorage.removeItem('type');
      dispatch(push('/'));
   }

}
