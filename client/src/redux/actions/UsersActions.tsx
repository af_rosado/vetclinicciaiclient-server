import axios from 'axios';

import * as types from '../Types';


export function getAllAdmins() {
   return function (dispatch: any) {
      const url = '/admins';

      axios({
         method: 'get',
         url
      })
         .then(response => {
            dispatch({
               type: types.GET_ALL_ADMINS,
               admins: response.data
            })
         })
         .catch(error => {
         })
   }
}

export function getAdmin(id: any) {
   return function (dispatch: any) {
      const url = `/admins/${id}`
      const token = localStorage.getItem('token')

      axios({
         method: 'get',
         url,
         headers: {
            'Authorization': `Bearer ${token}`
         }
      })
          .then(response => {
             dispatch({
                type: types.GET_ADMIN,
                admin: response.data
             })
          })
   }
}

export function delAdmin(id: number, callback: any) {
   return function (dispatch: any) {
      const url = '/admins/' + id;
      const token = localStorage.getItem('token')

      axios({
         method: 'delete',
         headers: {
            'Authorization': `Bearer ${token}`
         },
         url
      })
         .then(response => {

            if(response.status === 200){
               callback('Admin deleted successfuly', 'success')
               dispatch({
                  type: types.GET_ALL_ADMINS,
                  admins: response.data
               })
            }
         })
         .catch(error => {
            callback('Oops! There was an error deleting this Admin...', 'error')
         })
   }
}

export function getAllVets() {
   return function (dispatch: any) {
      const url = '/vets'

      axios({
         method: 'get',
         url
      })
         .then(response => {
            dispatch({
               type: types.GET_ALL_VETS,
               vets: response.data
            })
         })
         .catch(error => {
         })
   }
}

export function delVet(id: number, callback: any) {
   return function (dispatch: any) {
      const url = '/admins/vets/' + id;
      const token = localStorage.getItem('token')

      axios({
         method: 'put',
         headers: {
            'Authorization': `Bearer ${token}`
         },
         url
      })
         .then(response => {
            console.log(response)
            if(response.status === 200){
               callback('Vet deleted successfuly', 'success')
               dispatch(getAllVets())
            }
         })
         .catch(error => {
            callback('Oops! There was an error deleting this Vet...', 'error')
         })
   }
}

export function getAllClients() {
   return function (dispatch: any) {
      const url = '/clients'

      axios({
         method: 'get',
         url
      })
         .then(response => {
         })
         .catch(error => {
         })
   }
}

