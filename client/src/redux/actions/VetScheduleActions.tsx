import * as types from '../Types';
import moment, { Moment } from 'moment';
import Axios from 'axios';

export function saveVetSchedule(id: number, month: any, shifts: any, callback: any) {
   return function (dispatch: any) {
      let data: Array<any> = [];
      const m = moment(month).format("YYYY-MM");

      shifts.forEach((shift: any, day:number) => {
         let d = (day+1) < 10 ? `-0${day+1}` : `-${day+1}`;
         let startDate = `${m}${d}T${shift.start}`;
         let endDate = `${m}${d}T${shift.end}`;
         let msDiff = moment(shift.end, "HH:mm").diff(moment(shift.start, "HH:mm"));
         var duration = moment.duration(msDiff);
         var s = Math.floor(duration.asHours());
   
         if(s>0){
            data.push({
               "startDate": startDate,
               "endDate": endDate,
               "duration": s
            })
         }               
      });
      
      const url = '/admins/vets/'+id+'/schedule'
      const token = localStorage.getItem('token')
      Axios({
         method: 'post',
         url,
         headers: {
            'Authorization': `Bearer ${token}`
         },
         data
      })
         .then(response => {
            if(response.status === 200){
               callback('Vet Schedule saved', 'success')
            }
         })
         .catch(function (error) {
            if (error.response) {
               callback(error.response.data.message, 'error')
            } 
          });
   }
}

export function setVetToSchedule(vetId: number | null) {
   return {
      type: types.SET_VET_TO_SCHEDULE,
      vetId
   }
}

export function setVetScheduleMonth(month: Moment) {
   return {
      type: types.SET_VET_SCHEDULE_MONTH,
      month
   }
}

export function setVetShiftStart(index: number, start: any){
   return {
      type: types.SET_VET_SHIFT_START,
      index,
      start
   }
}

export function setVetShiftEnd(index: number, end: any){
   return {
      type: types.SET_VET_SHIFT_END,
      index,
      end
   }
}