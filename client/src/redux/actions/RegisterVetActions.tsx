import axios from 'axios';
import * as types from '../Types';


export function registVet(data: any, callback: any) {
   return function (dispatch: any) {
      const url = '/admins/vets'
      const token = localStorage.getItem('token')
      axios({
         method: 'post',
         url,
         headers: {
            'Authorization': `Bearer ${token}`
         },
         data
      })
         .then(response => {
            if(response.status === 200){
               callback('Vet registered successfuly', 'success')
            }
         })
         .catch(error => {
            callback('Oops! There was an error registering new Vet...', 'error')
         })
   }
}

export function setRegisterVetName(name: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_NAME,
         name
      })

   }
}

export function setRegisterVetEmail(email: string) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_EMAIL,
         email
      })
   }
}

export function setRegisterVetPassword(password: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_PASSWORD,
         password
      })
   }
}

export function setRegisterVetPhone(phone: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_PHONE,
         phone
      })
   }
}

export function setRegisterVetAddress(address: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_ADDRESS,
         address
      })
   }
}

export function setRegisterVetPicture(picture: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_VET_FORM_PHOTO,
         picture
      })
   }
}


