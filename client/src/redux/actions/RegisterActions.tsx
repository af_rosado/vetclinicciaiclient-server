import axios from 'axios';
import { submitLogin } from './LoginActions';
import * as types from '../Types';

export function registClient(data: any) {
   return function (dispatch: any) {
      const url = '/register/client'
      axios({
         method: 'post',
         url,
         data
      })
         .then(response => {
            if (response.status === 200) {
               let loginData = {
                  'email': data.email,
                  'password': data.password
               }
               dispatch(submitLogin(loginData))
            }
         })
         .catch(error => {
         })
   }
}

export function setRegisterName(name: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_NAME,
         name
      })
   }
}

export function setRegisterEmail(email: string) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_EMAIL,
         email
      })
   }
}

export function setRegisterPassword(password: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_PASSWORD,
         password
      })
   }
}

export function setRegisterPhone(phone: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_PHONE,
         phone
      })
   }
}

export function setRegisterAddress(address: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_ADDRESS,
         address
      })
   }
}

export function setRegisterPicture(picture: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_FORM_PHOTO,
         picture
      })
   }
}


