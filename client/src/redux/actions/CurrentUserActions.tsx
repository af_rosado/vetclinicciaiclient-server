import axios from 'axios';

import * as types from '../Types';
import { Pet } from '../../interfaces/Interfaces';

export function getUserDetails(id: string | null) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/clients/${id}`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER,
                    user: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function getUserPets(id: string | null) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/clients/${id}/pets`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER_PETS,
                    pets: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function getUserApts(id: string | null, type: string) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/${type}/${id}/apts`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER_APTS,
                    appointments: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function addPet(id: string|null, data: {}){
    return async function () {
        const token = localStorage.getItem('token');
        await axios.post(`/clients/${id}/pet`, data, { headers: { 'authorization': token } })
    }
}

export function removePet(id: string|null, pet: string|null){
    return async function () {
        const token = localStorage.getItem('token');
        await axios.put(`/clients/${id}/pet/${pet}`, { headers: { 'authorization': token } })
    }
}

export function getAdminDetails(id: string | null) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/admins/${id}`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER,
                    user: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function setToShowId(id: number) {
    return function (dispatch: any) {
        dispatch({
            type: types.SET_TO_SHOW_ID,
            toShowId: id
        })
    }
}
export function getVetDetails(id: string | null) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/vets/${id}`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER,
                    user: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function getVetSchedule(id: string | null) {
    return async function (dispatch: any) {
        const token = localStorage.getItem('token');
        await axios.get(`/vets/${id}/schedule`, { headers: { 'authorization': token } }).then(
            response => {
                dispatch({
                    type: types.GET_CURRENT_USER,
                    schedule: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function getAllShifts() {
    return async function (dispatch: any) {
        await axios.get(`/apts/shifts`).then(
            response => {
                console.log(response)
                dispatch({
                    type: types.GET_ALL_SHIFTS,
                    shifts: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function getShiftsOnDay(data: string) {
    return async function (dispatch: any) {
        await axios.get(`/apts/shiftsonday`, {
            params: {
                date: data
            }
        }).then(
            response => {
                console.log(response)
                dispatch({
                    type: types.GET_ALL_SHIFTS,
                    shifts: response.data
                })
            }
        ).catch(
            reason => {
                console.log(reason)
            }
        )
    }
}

export function updateUserDetails(id: string | null, newData: {}) {
    return async function () {
        const token = localStorage.getItem('token');
        await axios.put(`/clients/${id}`, newData, { headers: { 'authorization': token } })
    }
}

export function updateUserPicture(id: string | null, newData: {}) {
    return async function () {
        const token = localStorage.getItem('token');
        await axios.put(`/clients/${id}/picture`, newData, { headers: { 'authorization': token } })
    }
}

export function changeUserPassword(id: string | null, newData: {}) {
    return async function () {
        const token = localStorage.getItem('token');
        await axios.put(`/clients/${id}/password`, newData, { headers: { 'authorization': token } })
    }
}

export function toggleEdition(value: boolean) {
    return function (dispatch: any) {
        dispatch({
            type: types.TOGGLE_EDITION,
            edition: value
        });
    }
}

export function togglePasswordChange(value: boolean) {
    return function (dispatch: any) {
        dispatch({
            type: types.TOGGLE_PASSWORD_CHANGE,
            passwordChange: value
        });
    }
}

export function toggleAddPet(value: boolean) {
    return function (dispatch: any) {
        dispatch({
            type: types.TOGGLE_ADD_PET,
            addPet: value
        });
    }
}

export function toggleRemovePet(value: boolean) {
    return function (dispatch: any) {
        dispatch({
            type: types.TOGGLE_REMOVE_PET,
            removePet: value
        });
    }
}

export function toggleAddApt(value: boolean) {
    return function (dispatch: any) {
        dispatch({
            type: types.TOGGLE_ADD_APT,
            addApt: value
        });
    }
}

export function setPetProfile(value: Pet) {
    return function (dispatch: any) {
        dispatch({
            type: types.SET_PET_PROFILE,
            petProfile: value
        });
    }
}
