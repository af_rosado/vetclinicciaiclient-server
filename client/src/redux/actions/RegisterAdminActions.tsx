import axios from 'axios';
import * as types from '../Types';


export function registAdmin(data: any, callback: any) {
   return function (dispatch: any) {
      const url = '/register/admin'
      const token = localStorage.getItem('token')
      axios({
         method: 'post',
         url,
         headers: {
            'Authorization': `Bearer ${token}`
         },
         data
      })
         .then(response => {
            if(response.status === 200){
               callback('Admin registered successfuly', 'success')
            }

         })
         .catch(error => {
            callback('Oops! There was an error registering new Admin...', 'error')
         })
   }
}

export function setRegisterAdminName(name: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_NAME,
         name
      })

   }
}

export function setRegisterAdminEmail(email: string) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_EMAIL,
         email
      })
   }
}

export function setRegisterAdminPassword(password: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_PASSWORD,
         password
      })
   }
}

export function setRegisterAdminPhone(phone: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_PHONE,
         phone
      })
   }
}

export function setRegisterAdminAddress(address: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_ADDRESS,
         address
      })
   }
}

export function setRegisterAdminPicture(picture: String) {
   return function (dispatch: any) {
      dispatch({
         type: types.SET_REGISTER_ADMIN_FORM_PHOTO,
         picture
      })
   }
}


