import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'connected-react-router'
import thunk from 'redux-thunk';

import rootReducer from './rootReducer';
import {createBrowserHistory} from "history";

const initialState = {}

export const history = createBrowserHistory()

export const store = createStore(
  rootReducer(history),
  initialState,
  compose(
    applyMiddleware(
        routerMiddleware(history),
        thunk
    )
  )
)
