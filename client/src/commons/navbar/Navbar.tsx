import React, { useEffect } from 'react';
import Sticky from 'react-stickynode';
import { Grid, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { push } from 'connected-react-router';
import { useDispatch, useSelector } from "react-redux";
import Swal from 'sweetalert';
import { colors } from '../../constants';
import { logout } from "../../redux/actions/LoginActions";
var jwtDecode = require('jwt-decode');

const Navbar = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const id = useSelector((state: any) => state.AuthReducer.id);
   const token = localStorage.getItem('token');
   let decodedToken = null;
   let isAdmin = false
   if(token){
      decodedToken = jwtDecode(token);
      isAdmin = decodedToken.type === 'ADMIN'
   }


   const handleOnClick = () => {
      Swal({
         title: 'Are you sure?',
         buttons: ['Cancel', 'Ok']
      })
         .then(value => {
            if(value)
               dispatch(logout())
         });
   };

   useEffect(() => {
   }, [id, token])

   return (
      (!token) ?
         <Sticky enabled={true} bottomBoundary={1200} innerZ={10}>
            <Grid container direction='row' justify='space-between' alignItems="center" className={classes.mainContainer}>
               <Grid item xs={6} style={{ textAlign: 'left', paddingLeft: 25 }}>
                  <Typography variant="h6" style={{ letterSpacing: 2 }}>
                     <Link underline='none' onClick={() => dispatch(push('/'))} className={classes.navText}>
                        PAWS - VetClinic
                            </Link>
                  </Typography>
               </Grid>
               <Grid item xs={6} style={{ textAlign: 'right', paddingRight: 25 }}>
                  <Typography variant="h6">
                     <Link underline='none' onClick={() => (dispatch(push('/login')))} className={classes.navText}>
                        Login / Register
                            </Link>
                  </Typography>
               </Grid>
            </Grid>
         </Sticky>
         :
         <Sticky enabled={true} bottomBoundary={1200} innerZ={10}>
            <Grid container direction='row' justify='space-between' alignItems="center" className={classes.mainContainer}>
               <Grid item xs={6} style={{ textAlign: 'left', paddingLeft: 25 }}>
                  <Typography variant="h6" style={{ letterSpacing: 2 }}>
                     <Link underline='none' onClick={() => dispatch(push('/'))} className={classes.navText}>
                        PAWS - VetClinic
                     </Link>
                  </Typography>
               </Grid>
               <Grid item xs={6} style={{ textAlign: 'right', paddingRight: 25 }}>
                  <Grid container direction='row' justify='flex-end' alignItems='center' spacing={3}>
                     <Grid item xs={6} style={{ textAlign: 'right' }}>
                        <Typography variant="h6">
                           <Link underline='none' className={classes.navText} onClick={() => dispatch(push('/profile'))}>
                              My Profile
                           </Link>
                        </Typography>
                     </Grid>
                     { isAdmin && <Grid item xs={3} style={{ textAlign: 'right' }}>
                        <Typography variant="h6">
                           <Link underline='none' className={classes.navText} onClick={()=>dispatch(push('/admin/hireAdmin'))}>
                              Admin Menu
                           </Link>
                        </Typography>
                     </Grid>}
                     <Grid item xs={3} style={{ textAlign: 'right' }}>
                        <Typography variant="h6">
                           <Link underline='none' className={classes.navText} onClick={handleOnClick}>
                              Logout
                           </Link>
                        </Typography>
                     </Grid>
                  </Grid>
               </Grid>
            </Grid>
         </Sticky>
   );
}

export default Navbar;

const useStyles = makeStyles(() => ({
   mainContainer: {
      backgroundColor: colors.primary,
      height: 66
   },
   navText: {
      cursor: 'pointer',
      color: colors.white
   },
   '&:hover': {
      color: `${colors.primary} !important`
   }
}))
