import React from 'react';
import {
   makeStyles,
} from '@material-ui/core/styles';
import { Grid, Typography } from '@material-ui/core';
import { Animated } from 'react-animated-css';


const Header: React.FC = () => {

   const classes = useStyles();

   return (
      <Grid container direction='row' justify='center' alignItems='center' className={classes.header}>
         <Grid item xs={6} className={classes.logoHeader}>
            <Animated animationIn="zoomInDown" animationOut="fadeOutRightBig" animationInDuration={1400} animationOutDuration={1400} isVisible={true}>
               <img src="/img/paw.png" alt='icon' width={100}/>
            </Animated>
         </Grid>
         <Grid item xs={6} className={classes.titleHeader}>
            <Typography variant='h3' style={{fontFamily: "font-family: 'Raleway', sans-serif"}}>
               PAWS
            </Typography>
         </Grid>
      </Grid>
);
}

export default Header;


const useStyles = makeStyles(theme => ({
   header: {
      paddingTop: 20,
      paddingBottom: 20,
      backgroundColor: '#000441'
   },
   logoHeader: {
      textAlign: 'right',
      paddingRight: 20
   },
   titleHeader: {
      textAlign: 'left',
      paddingLeft: 20,
      color: 'white'
   }
}))