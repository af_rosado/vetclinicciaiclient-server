import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Container, Typography } from '@material-ui/core';
import AdminMenu from './components/AdminMenu';
import AdminRouter from '../../navigation/AdminRouter';
import {useDispatch, useSelector } from 'react-redux';
import { getAdmin } from "../../redux/actions/UsersActions";

interface IProps {
   id: string
}

const Admin = ({id} : IProps) => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const admin = useSelector((state: any) => state.UsersReducer.admin);

   const currentId = localStorage.getItem('id')

   useEffect(() => {
      id ? dispatch(getAdmin(id)) : dispatch(getAdmin(currentId))
   },[])

   return (
      <Container style={{paddingRight: 0, paddingLeft: 0}}>
         <Grid container direction='row' justify='space-between' spacing={10}>
            <Grid item xs={3}>
               <Grid container direction='row' justify='center' alignItems='center' spacing={6}>
                  <Grid item xs={12} className={classes.grids}>
                     <img src={admin.picture} width={250} height={250}/>
                  </Grid>
                  <Grid item xs={12} className={classes.grids}>
                     <AdminMenu/>
                  </Grid>
               </Grid>
            </Grid>
            <Grid item xs={9} className={classes.grids}>
               <AdminRouter/>
            </Grid>
         </Grid>

      </Container>
   )
}

export default Admin;

const useStyles = makeStyles(theme => ({
   logoHeader: {
      textAlign: 'right',
      paddingRight: 20
   },
   titleHeader: {
      textAlign: 'left',
      paddingLeft: 20
   },
   grids: {
      textAlign: 'center'
   }
}))
