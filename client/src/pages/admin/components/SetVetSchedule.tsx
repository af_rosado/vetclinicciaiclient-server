import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Paper, Typography, Button, TextField } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { getAllVets } from '../../../redux/actions/UsersActions';
import { useSnackbar } from 'notistack';
import { setVetToSchedule, setVetScheduleMonth, setVetShiftStart, setVetShiftEnd, saveVetSchedule } from '../../../redux/actions/VetScheduleActions';
import SetVetScheduleSelectVet from './SetVetScheduleSelectVet';
import { DatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from '@date-io/moment';
import moment, { Moment } from 'moment';


const SetVetSchedule = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const vetId = useSelector((state: any) => state.SetVetScheduleReducer.vetId);
   const selectedDate = useSelector((state: any) => state.SetVetScheduleReducer.month);
   const shifts = useSelector((state: any) => state.SetVetScheduleReducer.shifts);
   const { enqueueSnackbar } = useSnackbar();


   const handeSnackbarEnqueue = (message: string, type: "default" | "error" | "success" | "warning" | "info" | undefined) => {
      enqueueSnackbar(message, { variant: type });
   }

   const handleDateChange = (date: any) => {
      dispatch(setVetScheduleMonth(moment(date)))
   }

   useEffect(() => {
      dispatch(getAllVets());
   }, []);

   return (
      <Paper>
         <Grid container spacing={3}>
            {
               vetId === null &&
               <SetVetScheduleSelectVet />
            }
            {
               vetId &&
               <React.Fragment>
                  <Grid item xs={12}>
                     <Button variant='outlined' fullWidth onClick={() => dispatch(setVetToSchedule(null))}>
                        Select Another Vet
                     </Button>
                  </Grid>
                  <Grid item xs={12}>
                     <Grid container alignItems='center' justify='center'>
                        <Grid item xs={4}>
                           <MuiPickersUtilsProvider utils={MomentUtils}>
                              <DatePicker
                                 variant="static"
                                 openTo="year"
                                 views={["year", "month"]}
                                 label="Select a month"
                                 helperText="Set the Vet's Schedule for this month"
                                 value={selectedDate}
                                 onChange={handleDateChange}
                              />
                           </MuiPickersUtilsProvider>
                        </Grid>
                     </Grid>
                     <Grid container alignItems='center' justify='flex-start' spacing={2}>
                        {                           
                           [...Array(selectedDate.daysInMonth())].map((day, i) => 
                           <Grid item key={i} xs={3}>
                              Day {i+1}:
                              <TextField
                                 className={classes.dateInput}
                                 label='Start Time'
                           
                                 defaultValue="07:00"
                                 fullWidth
                                 type='time'
                                 onChange={(event)=>dispatch(setVetShiftStart(i, event.target.value))}
                              />
                              <TextField
                                 className={classes.dateInput}
                                 label='End Time'
                                 defaultValue="12:00"
                                 fullWidth
                                 type='time'
                                 onChange={(event)=>dispatch(setVetShiftEnd(i, event.target.value))}
                              />
                           </Grid>)
                        }                       
                     </Grid>
                  </Grid>
                  <Grid item xs={12}>
                     <Button 
                        variant='outlined' 
                        onClick={()=>dispatch(saveVetSchedule(vetId, selectedDate, shifts, handeSnackbarEnqueue))} 
                        fullWidth
                     >
                        Save Schedule
                     </Button>
                  </Grid>
               </React.Fragment>
            }
         </Grid>
      </Paper>
   )
}

export default SetVetSchedule;



const useStyles = makeStyles(theme => ({
   grids: {
      textAlign: 'center',
      marginTop: 15,
      marginBottom: 15
   },
   dateInput: {
      marginTop: 10
   }
}))