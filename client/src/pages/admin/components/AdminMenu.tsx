import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Button, Paper } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { push } from 'connected-react-router';


const AdminMenu = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const { page } = useParams()

   return (
      <Paper>
         <Grid container direction='column' spacing={1}>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'hireAdmin'}
                  onClick={() => dispatch(push('/admin/hireAdmin'))}
                  fullWidth
               >
                  Hire Admin
               </Button>
            </Grid>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'fireAdmin'}
                  onClick={() => dispatch(push('/admin/fireAdmin'))}
                  fullWidth
               >
                  Fire Admin
               </Button>
            </Grid>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'hireVet'}
                  onClick={() => dispatch(push('/admin/hireVet'))}
                  fullWidth
               >
                  Hire Veterinarian
               </Button>
            </Grid>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'fireVet'}
                  onClick={() => dispatch(push('/admin/fireVet'))}
                  fullWidth
               >
                  Fire Veterinarian
               </Button>

            </Grid>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'setVetSchedule'}
                  onClick={() => dispatch(push('/admin/setVetSchedule'))}
                  fullWidth
               >
                  Set Veterinarian Schedule
               </Button>

            </Grid>
            <Grid item>
               <Button
                  variant='outlined'
                  color='primary'
                  disabled={page === 'checkVetAppts'}
                  onClick={() => dispatch(push('/admin/checkVetAppts'))}
                  fullWidth
               >
                  Check Veterinarian Appointments
               </Button>
            </Grid>
         </Grid>
      </Paper>

   )
}

export default AdminMenu;

const useStyles = makeStyles(theme => ({

}))
