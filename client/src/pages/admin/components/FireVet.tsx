import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Paper, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { employee } from '../../home';
import { getAllVets, delVet } from '../../../redux/actions/UsersActions';
import ActionEmployeeCard from './ActionEmployeeCard';
import { useSnackbar } from 'notistack';


const FireVet = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const vets = useSelector((state: any) => state.UsersReducer.vets);
   const { enqueueSnackbar } = useSnackbar();

   const handeSnackbarEnqueue = (message:string, type: "default" | "error" | "success" | "warning" | "info" | undefined) =>{
      enqueueSnackbar(message, {variant:type});
   }

   useEffect(() => {
      dispatch(getAllVets());
   }, []);

   return (
      <Paper>
         <Grid container >
            {
               vets.length === 0 &&
               <Grid item xs={12}>
                  <Typography variant='h6' align='center'>
                     There are currently no Vets in the System
                  </Typography>
               </Grid>
            }
            {vets.map((vet: employee, index: number) =>
               <Grid key={index} item xs={4} className={classes.grids} style={{ marginBottom: 20, marginTop: 20 }}>
                  <ActionEmployeeCard
                     fireEvent={() => dispatch(delVet(vet.id, handeSnackbarEnqueue))}
                     employee={vet}
                     title="Fire"
                  />
               </Grid>
            )}
         </Grid>
         
      </Paper>
   )
}

export default FireVet;

const useStyles = makeStyles(theme => ({
   grids: {
      textAlign: 'center',
      marginTop: 15,
      marginBottom: 15
   },
}))