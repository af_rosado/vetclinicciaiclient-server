import React from 'react';
import { Typography, Grid, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { colors } from '../../../constants';
import { employee } from '../../home';

interface IProps {
   employee: employee,
   fireEvent: VoidFunction,
   title: string
}

const ActionEmployeeCard = ({ employee, fireEvent, title }: IProps) => {
   const classes = useStyles();

   return (
      <React.Fragment>
         <Grid container direction='row' justify={'center'} alignItems='center' spacing={1}>
            <Grid item xs={12}>
               <img src={employee.picture ? employee.picture : '/img/default-profile-picture.jpg'}
                  alt='user photo' width='200' height='200'
               />
            </Grid>
            <Grid item xs={12}>
               <Typography variant='subtitle1' className={classes.userInfo}>
                  {employee.name}
               </Typography>
            </Grid>
            <Grid item xs={12}>
               <Button onClick={fireEvent} variant='outlined'>
                  {title} {employee.name}
               </Button>
            </Grid>
         </Grid>
      </React.Fragment>
   )
}

export default ActionEmployeeCard;

const useStyles = makeStyles(() => ({
   userInfo: {
      color: colors.greyText,
      marginTop: 10
   }
}))
