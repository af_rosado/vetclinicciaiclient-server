import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Paper, TextField, Button} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { setRegisterAdminName, setRegisterAdminEmail,
   setRegisterAdminPassword, setRegisterAdminPhone,
   setRegisterAdminAddress, setRegisterAdminPicture, registAdmin } from '../../../redux/actions/RegisterAdminActions';
import { useSnackbar } from 'notistack';


const HireAdmin = () => {
   const classes = useStyles();
   const form = useSelector((state: any) => state.RegisterAdminReducer);
   const dispatch = useDispatch();
   const { enqueueSnackbar } = useSnackbar();

   const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminName(event.target.value));
   };

   const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminEmail(event.target.value));
   };

   const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminPassword(event.target.value));
   };

   const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminPhone(event.target.value));
   };

   const handleAddressChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminAddress(event.target.value));
   };

   const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAdminPicture(event.target.value));
   };

   const handeSnackbarEnqueue = (message:string, type: "default" | "error" | "success" | "warning" | "info" | undefined) =>{
      enqueueSnackbar(message, {variant:type});
      //return true;
   }


   return (
      <Paper>
          <Grid container direction='column' spacing={2}>
         <Grid item xs={12}>
            <TextField
               label="Name"
               value={form.name}
               onChange={handleNameChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Email"
               value={form.email}
               onChange={handleEmailChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Password"
               type='password'
               value={form.password}
               onChange={handlePasswordChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Phone"
               value={form.phone}
               onChange={handlePhoneChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Address"
               value={form.address}
               onChange={handleAddressChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Picture (link)"
               value={form.picture}
               onChange={handlePictureChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <Button
               variant="contained" color="primary"
               className={classes.Button} fullWidth
               onClick={()=>dispatch(registAdmin(form, handeSnackbarEnqueue))}
            >
               Hire New Admin
            </Button>
         </Grid>
      </Grid>
      </Paper>
   )

}
export default HireAdmin;


const useStyles = makeStyles(theme => ({
   Button: {
      marginTop: 16
   },
}))
