import React from 'react';
import { Paper, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core";
import { useSelector } from "react-redux";

const AdminInfo = () => {
  const classes = useStyles();
  const admin = useSelector((state: any) => state.UsersReducer.admin);

  return(
     <Paper>
       <Grid container direction='row'>
         <Grid item xs={12} style={{textAlign: 'center', marginBottom: 75}}>
          <Typography variant='h3'>
            Profile
          </Typography>
         </Grid>
         <Grid item xs={6}>
           <Grid container direction='column' alignItems='flex-end' style={{paddingRight: 30}} spacing={3}>
            <Grid item xs={8} className={classes.leftGrids}>
              <Typography variant='h6'>
                Name:
              </Typography>
            </Grid>
             <Grid item xs={8} className={classes.leftGrids}>
               <Typography variant='h6'>
                 Email:
               </Typography>
             </Grid>
             <Grid item xs={8} className={classes.leftGrids}>
               <Typography variant='h6'>
                 Phone:
               </Typography>
             </Grid>
             <Grid item xs={8} className={classes.leftGrids}>
               <Typography variant='h6'>
                 Address:
               </Typography>
             </Grid>
           </Grid>
         </Grid>
         <Grid item xs={6}>
           <Grid container direction='column' alignItems='flex-start' style={{marginLeft: 0}} spacing={3}>
            <Grid item xs={8} className={classes.rightGrids}>
              <Typography variant='h6'>
                {admin.name}
              </Typography>
            </Grid>
             <Grid item xs={8} className={classes.rightGrids}>
               <Typography variant='h6'>
                 {admin.email}
               </Typography>
             </Grid>
             <Grid item xs={8} className={classes.rightGrids}>
               <Typography variant='h6'>
                 {admin.phone}
               </Typography>
             </Grid>
             <Grid item xs={8} className={classes.rightGrids}>
               <Typography variant='h6'>
                 {admin.address}
               </Typography>
             </Grid>
           </Grid>
         </Grid>
       </Grid>
     </Paper>
  )
};

export default AdminInfo;

const useStyles = makeStyles(() => ({
  leftGrids: {
    textAlign: 'right'
  },
  rightGrids: {
    textAlign: 'left'
  }
}))
