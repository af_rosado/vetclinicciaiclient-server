import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Paper } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { employee } from '../../home';
import { getAllAdmins, delAdmin } from '../../../redux/actions/UsersActions';
import ActionEmployeeCard from './ActionEmployeeCard';
import { useSnackbar } from 'notistack';


const FireAdmin = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const admins = useSelector((state: any) => state.UsersReducer.admins);
   const { enqueueSnackbar } = useSnackbar();

   const handeSnackbarEnqueue = (message:string, type: "default" | "error" | "success" | "warning" | "info" | undefined) =>{
      enqueueSnackbar(message, {variant:type});
   }

   useEffect(() => {
      dispatch(getAllAdmins());
   }, []);

   return (
      <Paper>
         <Grid container >
            {admins.map((admin: employee, index: number) =>
               <Grid key={index} item xs={4} className={classes.grids} style={{ marginBottom: 20, marginTop: 20 }}>
                  <ActionEmployeeCard
                     fireEvent={() => dispatch(delAdmin(admin.id, handeSnackbarEnqueue))}
                     employee={admin}
                     title="Fire"                     
                  />
               </Grid>
            )}
         </Grid>
         
      </Paper>
   )
}

export default FireAdmin;

const useStyles = makeStyles(theme => ({
   grids: {
      textAlign: 'center',
      marginTop: 15,
      marginBottom: 15
   },
}))