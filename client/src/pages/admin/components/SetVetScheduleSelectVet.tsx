import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Typography } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { employee } from '../../home';
import ActionEmployeeCard from './ActionEmployeeCard';
import { setVetToSchedule } from '../../../redux/actions/VetScheduleActions';


const SetVetScheduleSelectVet = () => {
   const classes = useStyles();
   const dispatch = useDispatch();
   const vets = useSelector((state: any) => state.UsersReducer.vets);

   return (
      <React.Fragment>
         {
            vets.length === 0 &&
            <Grid item xs={12}>
               <Typography variant='h6' align='center'>
                  There are currently no Vets in the System
               </Typography>
            </Grid>
         }
         {vets.map((vet: employee, index: number) =>
            <Grid key={index} item xs={4} className={classes.grids} style={{ marginBottom: 20, marginTop: 20 }}>
               <ActionEmployeeCard
                  fireEvent={() => { dispatch(setVetToSchedule(vet.id)) }}
                  employee={vet}
                  title="Select"
               />
            </Grid>
         )}
      </React.Fragment>
   )
}

export default SetVetScheduleSelectVet;



const useStyles = makeStyles(theme => ({
   grids: {
      textAlign: 'center',
      marginTop: 15,
      marginBottom: 15
   },
}))