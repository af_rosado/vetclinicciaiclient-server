import React from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Paper, TextField, Button} from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import { setRegisterVetName, setRegisterVetEmail, 
   setRegisterVetPassword, setRegisterVetPhone, 
   setRegisterVetAddress, setRegisterVetPicture, registVet } from '../../../redux/actions/RegisterVetActions';
import { useSnackbar } from 'notistack';


const HireVet = () => {
   const classes = useStyles();
   const form = useSelector((state: any) => state.RegisterVetReducer);
   const dispatch = useDispatch();
   const { enqueueSnackbar } = useSnackbar();

   const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetName(event.target.value));
   };

   const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetEmail(event.target.value));
   };

   const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetPassword(event.target.value));
   };

   const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetPhone(event.target.value));
   };

   const handleAddressChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetAddress(event.target.value));
   };

   const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterVetPicture(event.target.value));
   };   

   const handeSnackbarEnqueue = (message:string, type: "default" | "error" | "success" | "warning" | "info" | undefined) =>{
      enqueueSnackbar(message, {variant:type});
      //return true;
   }
  

   return (
      <Paper>
          <Grid container direction='column' spacing={2}>
         <Grid item xs={12}>
            <TextField
               label="Name"
               value={form.name}
               onChange={handleNameChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Email"
               value={form.email}
               onChange={handleEmailChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Password"
               type='password'
               value={form.password}
               onChange={handlePasswordChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Phone"
               value={form.phone}
               onChange={handlePhoneChange}
               fullWidth
            />
         </Grid>         
         <Grid item xs={12}>
            <TextField
               label="Address"
               value={form.address}
               onChange={handleAddressChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Picture (link)"
               value={form.picture}
               onChange={handlePictureChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <Button 
               variant="contained" color="secondary" 
               className={classes.Button} fullWidth
               onClick={()=>dispatch(registVet(form, handeSnackbarEnqueue))}
            >
               Hire New Vet
            </Button>
         </Grid>
      </Grid>
      </Paper>
   )

}
export default HireVet;


const useStyles = makeStyles(theme => ({
   Button: {
      marginTop: 16
   },
}))