import React, { useEffect } from 'react';
import { useDispatch } from "react-redux";
import { Container, Grid, Typography, Paper } from '@material-ui/core';
import { Animated } from "react-animated-css";
import { makeStyles } from "@material-ui/styles";

import { registFirstAdmin } from "../../redux/actions/LoginActions";
import { getAllAdmins, getAllVets } from "../../redux/actions/UsersActions";
import EmployeeTypeSelect from "./components/EmployeeTypeSelect";
import Header from "./components/Header";

export interface employee {
   id: number,
   picture: string,
   name: string
}

const Home = () => {
   const dispatch = useDispatch();
   const classes = useStyles();

   useEffect(() => {
      dispatch(registFirstAdmin());
      dispatch(getAllAdmins());
      dispatch(getAllVets())
   }, [])

   return (
      <Container>
         <Paper>
            
          <Header/>
         </Paper>
         <Paper style={{marginTop: 46}}>
         <EmployeeTypeSelect/>
         </Paper>
       
      </Container>
   )
}

const useStyles = makeStyles(theme => ({
   logoHeader: {
      textAlign: 'right',
      paddingRight: 20
   },
   titleHeader: {
      textAlign: 'left',
      paddingLeft: 20
   }
}));

export default Home;
