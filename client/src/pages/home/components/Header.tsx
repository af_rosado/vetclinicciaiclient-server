import React from "react";
import { Grid, Typography } from '@material-ui/core';
import { Animated } from "react-animated-css";

import { colors } from '../../../constants';

const Header = () => {
    return(
        <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item xs={4} style={{textAlign: 'right', marginRight: 20}}>
                <Animated animationIn="zoomInDown" animationOut="fadeOutRightBig" animationInDuration={1600} animationOutDuration={1400} isVisible={true}>
                    <img src="/img/paw.png" alt='paws logo' width={200} height={200}/>
                </Animated>
            </Grid>
            <Grid item xs={4} style={{marginLeft: 20, color: colors.greyText}}>
                <Typography variant='h1'>
                    <b>PAWS</b>
                </Typography>
            </Grid>
        </Grid>
    )
}

export default Header;
