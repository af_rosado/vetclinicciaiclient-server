import React from 'react';
import { makeStyles } from "@material-ui/styles";
import {Paper, Typography, Grid, Card } from "@material-ui/core";
import { useSelector } from "react-redux";

import EmployeeCardContent from "./EmployeeCardContent";
import { employee } from "../index";
import { colors } from '../../../constants';

var Scroll = require('react-scroll');
var Element = Scroll.Element;
var scroller = Scroll.scroller;

const EmployeeTypeSelect = () => {
    const classes = useStyles();
    const admins = useSelector((state: any) => state.UsersReducer.admins);
    const vets = useSelector((state: any) => state.UsersReducer.vets);

    const [userType, setUserType] = React.useState(0);

    const handleOnClick = (type: number) => {
        setUserType(type);
        scroller.scrollTo('scrollHere', {
            duration: 1000,
            delay: 100,
            smooth: true,
            offset: 50
        })
    }

    return(
        <div className={classes.paper}>
            <Element name="scrollHere">
            <Grid container direction='row' justify='center' alignItems='center' className={classes.innerContainers}>
                <Grid item xs={12} className={classes.grids}>
                    <Typography variant='h3' className={classes.staffHeader}>
                        <b>Get to know our staff</b>
                    </Typography>
                </Grid>
            </Grid>
            <Grid container direction='row' justify='center' alignItems='center' className={classes.innerContainers}>
                <Grid item xs={6} className={classes.grids}>
                    <Typography variant='h6' className={userType === 0 ? classes.userTypeTextSelected : classes.userTypeText} onClick={() => handleOnClick(0)}>
                        Vets
                    </Typography>
                </Grid>
                <Grid item xs={6} className={classes.grids}>
                    <Typography variant='h6' className={userType === 1 ? classes.userTypeTextSelected : classes.userTypeText}  onClick={() => handleOnClick(1)}>
                        Administrators
                    </Typography>
                </Grid>
                <Grid container direction='row' justify='flex-start' alignItems='center' spacing={3} className={classes.innerContainers}>
                    {userType === 0 &&
                    <React.Fragment>
                        {vets.map((vet: employee, index: number) =>
                            <Grid item xs={4} className={classes.grids} style={{marginBottom: 20, marginTop: 20}}>
                                    <EmployeeCardContent employee={vet} key={index}/>
                            </Grid>
                        )}
                    </React.Fragment>
                    }
                    {userType === 1 &&
                    <React.Fragment>
                        {admins.map((admin: employee, index: number) =>
                            <Grid item xs={4} className={classes.grids} style={{marginBottom: 20, marginTop: 20}}>
                                    <EmployeeCardContent employee={admin} key={index}/>
                            </Grid>
                            )}
                    </React.Fragment>
                    }
                </Grid>
            </Grid>
            </Element>
        </div>
    )
}

const useStyles = makeStyles(theme => ({
    staffHeader: {
        textTransform: 'uppercase',
        marginTop: 10,
        color: colors.primary
    },
    grids: {
        textAlign: 'center',
        marginTop: 15,
        marginBottom: 15
    },
    paper: {
        paddingLeft: 15,
        paddingRight: 15,
        //backgroundColor: "rgba(255,255,255,0.8)"
    },
    userTypeText: {
        cursor: 'pointer',
        width: '50%',
        margin: '0 auto',
        color: colors.greyText
    },
    userTypeTextSelected: {
        cursor: 'pointer',
        width: '50%',
        margin: '0 auto',
        backgroundColor: colors.primary,
        color: colors.white
    },
    innerContainers: {
        marginTop: 50
    }
}));

export default EmployeeTypeSelect;
