import React from 'react';
import { Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { push } from 'connected-react-router';
import { useDispatch } from "react-redux";

import { employee } from '../index';
import { colors } from '../../../constants';
import { setToShowId } from "../../../redux/actions/CurrentUserActions";

interface IProps {
    employee: employee
}

const EmployeeCardContent = ({employee}: IProps) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const handleOnClick = (id: number) => {
        dispatch(setToShowId(id))
        dispatch(push('/profile'));
    }

    return (
       <React.Fragment>
            <Grid container direction='row' justify={'center'} alignItems='center'>
                <Grid item xs={12}>
                    <img src={employee.picture ? employee.picture : '/img/default-profile-picture.jpg'}
                         alt='user photo' width='200' height='200' className={classes.picture}
                         onClick={() => handleOnClick(employee.id)}
                    />
                </Grid>
                <Grid item xs={6}>
                    <Typography variant='subtitle1' className={classes.userInfo}>
                        {employee.name}
                    </Typography>
                </Grid>
            </Grid>
       </React.Fragment>
    )
}

export default EmployeeCardContent;

const useStyles = makeStyles(() => ({
    userInfo: {
        color: colors.greyText,
        marginTop: 10
    },
    picture: {
        cursor: 'pointer',
        '&:hover': {
            opacity: '0.5'
        }
    }
}))
