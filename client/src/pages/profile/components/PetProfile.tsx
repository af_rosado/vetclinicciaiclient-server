import React from 'react';
import { Grid, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import CSS from 'csstype';

import { colors } from '../../../constants';
import { Pet, emptyPet } from '../../../interfaces/Interfaces';
import { useSelector, useDispatch } from 'react-redux';
import { setPetProfile } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(theme => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        fontSize: '2.5vh'
    },
    dados: {
        textAlign: 'left'
    }

}));

const imgStyle: CSS.Properties = {
    maxWidth: '200px',
    border: '2',
    borderStyle: 'solid',
    borderColor: colors.primary
};

interface IProps {
    pet: Pet
}

const PetProfile = (props: IProps) => {
    const classes = useStyle();
    const dispatch = useDispatch();

    let { name, species, age, description, status, picture } = props.pet;
    const petP = useSelector((state: any) => state.CurrentUserReducer.PetProfile);

    const handleCancel = () => {
        dispatch(setPetProfile(emptyPet));
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            <Grid container alignItems='center'>
                <Grid item xs={4} style={{ textAlign: 'left', }}>
                    <img
                        src={picture.includes("http") ? picture : '/img/default-profile-picture.jpg'}
                        style={imgStyle}
                    />
                </Grid>
                <Grid item xs={8} >
                    <Grid container direction='column' justify='space-evenly' style={{ minHeight: '200px' }}>
                        <Grid item xs={4}>
                            <Grid container spacing={2} alignItems='center' >
                                <Grid item xs={6}>
                                    <Typography className={classes.category}>Name: </Typography>
                                </Grid>
                                <Grid item xs={5}>
                                    <Typography className={classes.dados}>{name}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <Grid container spacing={2} alignItems='center'>
                                <Grid item xs={6}>
                                    <Typography className={classes.category}>Species: </Typography>
                                </Grid>
                                <Grid item xs={5}>
                                    <Typography className={classes.dados}>{species}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={4}>
                            <Grid container spacing={2} alignItems='center'>
                                <Grid item xs={6}>
                                    <Typography className={classes.category}>Age: </Typography>
                                </Grid>
                                <Grid item xs={5}>
                                    <Typography className={classes.dados}>{age}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid container direction='column'>
                    <Grid item xs={4} style={{ marginTop: '1vh', textAlign: 'left' }}>
                        <Grid container spacing={2}  >
                            <Grid item xs={5}>
                                <Typography className={classes.category}>Description: </Typography>
                            </Grid>
                            <Grid item xs={7}>
                                <Typography>{description}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item xs={4} style={{ marginTop: '1vh', textAlign: 'left' }}>
                        <Grid container spacing={2} >
                            <Grid item xs={5}>
                                <Typography className={classes.category}>Status: </Typography>
                            </Grid>
                            <Grid item xs={7}>
                                <Typography>{status}</Typography>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container justify='flex-end' style={{ marginTop: '2vh', padding: '1vw', paddingTop: 0 }}>
                <Grid item xs={12}>
                    <Grid container justify='flex-end'>
                        <Grid item xs={3} style={{ textAlign: 'center', border: 1, borderStyle: 'solid', backgroundColor: colors.primary, padding: 5 }}>
                            <Link
                                style={{ color: colors.white }}
                            onClick={handleCancel}
                            >
                                Back
                    </Link>
                        </Grid>
                    </Grid>
                </Grid>

            </Grid>
        </Grid>
    );
};

export default PetProfile;