import React from 'react';
import { Grid, Typography, Link, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { toggleRemovePet, removePet } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(theme => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: 10
    }
}));

const RemovePet = () => {
    const dispatch = useDispatch();
    const classes = useStyle();

    let userId = localStorage.getItem('id');
    let pets = useSelector((state: any) => state.CurrentUserReducer.pets);
    const remPet = useSelector((state: any) => state.CurrentUserReducer.removePet);

    const handleCancel = () => {
        dispatch(toggleRemovePet(!remPet));
    }

    const handleRemovePet = (id: string) => {
        dispatch(removePet(userId, id));
        dispatch(toggleRemovePet(!remPet));
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>

            <Typography variant="h5" style={{ marginTop: '2vh' }}>Remove Pet</Typography>

            <Grid container spacing={2} justify='space-between' style={{ marginTop: '2vh', marginLeft: '1vw' }}>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Pets:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    {
                        pets[0] === emptyPet
                            ? <Typography>You have no pets registered</Typography>
                            : renderPetList(pets, handleRemovePet)
                    }
                </Grid>
            </Grid>
            <Grid container justify='space-between' style={{ marginTop: '4vh', padding: '1vw', paddingTop: 0 }}>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleCancel}
                    >
                        Cancel
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

const renderPetList = (pets: [Pet], handleRemovePet: any) => {
    let arr: JSX.Element[] = [];
    pets.map(p => {
        arr.push(
            <Grid key={Math.random() * 8} container justify='space-between'>
                <Grid item xs={4}>
                    <Typography >
                        {p.name}
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Link
                        style={{ color: colors.primary }}
                        onClick={() => handleRemovePet(p.id)}
                    >
                        (Remove pet)
                    </Link>
                </Grid>
            </Grid>

        );
    });
    return arr;
}

export default RemovePet;
