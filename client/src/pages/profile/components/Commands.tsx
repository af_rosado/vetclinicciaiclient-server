import React from 'react';
import { Grid, Typography, Link, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { push } from 'connected-react-router';

import { colors } from '../../../constants';
import { toggleEdition, togglePasswordChange, toggleAddPet, toggleRemovePet, toggleAddApt } from '../../../redux/actions/CurrentUserActions';
import { useDispatch, useSelector } from 'react-redux';

interface IProps {
    userType: string | null
}

const useStyle = makeStyles(theme => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    commandsText: {
        cursor: 'pointer',
        '&:hover': {
            color: colors.greyText
        }
    }
}));

const Commands = (props: IProps) => {
    const { userType } = props;
    const dispatch = useDispatch();
    const classes = useStyle();

    const edition = useSelector((state: any) => state.CurrentUserReducer.edition);
    const passwordChange = useSelector((state: any) => state.CurrentUserReducer.passwordChange);
    const addPet = useSelector((state: any) => state.CurrentUserReducer.addPet);
    const remPet = useSelector((state: any) => state.CurrentUserReducer.removePet);
    const addApt = useSelector((state: any) => state.CurrentUserReducer.addApt);

    const userCommands: string[] = ["Edit Profile", "Change Password", "Register a pet", "Remove a pet", "Book Appointment"];
    const vetCommands: string[] = ["Check Schedule", "Complete Appointment"];
    const adminCommands: string[] = ["Go to dashboard"];

    let commands: any = [];
    let temp: string[] = [];

    switch (userType) {
        case 'CLIENT': temp = userCommands; break;
        case 'VET': temp = vetCommands; break;
        case 'ADMIN': temp = adminCommands; break;
    }

    const redirect = (c: string) => {
        switch (c) {
            case 'Edit Profile':
                if (!passwordChange && !addPet && !remPet && !addApt) {
                    dispatch(toggleEdition(!edition));
                }
                break;
            case 'Change Password':
                if (!edition && !addPet && !remPet && !addApt) {
                    dispatch(togglePasswordChange(!passwordChange));
                }
                break;
            case 'Register a pet':
                if (!edition && !passwordChange && !remPet && !addApt) {
                    dispatch(toggleAddPet(!addPet));
                }
                break;
            case 'Remove a pet':
                if (!edition && !passwordChange && !addPet && !addApt) {
                    dispatch(toggleRemovePet(!remPet));
                }
                break;
            case 'Book Appointment':
                if (!edition && !passwordChange && !addPet && !remPet) {
                    dispatch(toggleAddApt(!addApt));
                }
                break;
            case 'Go to dashboard':
                dispatch(push('/admin/hireAdmin'));
                break
            default:
                break;
        }
    }

    temp.map(c =>
        commands.push(
         <Grid item xs={12}>
           <Button fullWidth variant='outlined'  key={Math.random() * 8} onClick={() => redirect(c)}>
               <Typography className={classes.commandsText}>
                    {c}
               </Typography>
           </Button>
         </Grid>  
        ));

    return (
        <Grid container spacing={2} style={{marginTop: 24}}>
            {commands}
        </Grid>
    );
}

export default Commands;
