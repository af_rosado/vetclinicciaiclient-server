import React from 'react';
import {Grid, Typography, Link, TextField, Button} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { updateUserDetails, toggleEdition, updateUserPicture } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(theme => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: '1vw'
    }
}));

interface IProps {
    user: User | Vet | Client,
}

const EditDetails = (props: IProps) => {
    const {
        user,
    } = props;

    const dispatch = useDispatch();
    const classes = useStyle();

    let userId = localStorage.getItem('id');
    const edition = useSelector((state: any) => state.CurrentUserReducer.edition);
    let { name, email, phone, address, picture } = user;

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        name = event.target.value;
    }

    const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        email = event.target.value;
    }

    const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        phone = event.target.value;
    }

    const handleAddressChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        address = event.target.value;
    }

    const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        picture = event.target.value;
    }

    const handleChangesSubmission = () => {
        if (picture !== user.picture) {
            let picData: {} = {
                picture: picture
            }
            dispatch(updateUserPicture(userId, picData));
            dispatch(toggleEdition(!edition));
        } else {
            let newData: {} = {
                name: name,
                email: email,
                phone: phone,
                address: address,
                picture: picture
            }
            dispatch(updateUserDetails(userId, newData));
            dispatch(toggleEdition(!edition));
        }

    }

    const handleCancel = () => {
        dispatch(toggleEdition(!edition))
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            {
                <Typography variant="h5" style={{ marginTop: '2vh' }}>Edit Profile</Typography>
            }
            <Grid container spacing={2} style={{ marginTop: '2vh' }} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Name:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={name}
                        onChange={handleNameChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Email:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={email}
                        onChange={handleEmailChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between' >
                <Grid item xs={1} >
                    <Typography className={classes.category}>Phone:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={phone}
                        onChange={handlePhoneChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Address:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={address}
                        onChange={handleAddressChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Picture:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={picture}
                        onChange={handlePictureChange}
                    />
                </Grid>
            </Grid>
            <Grid container justify='space-between' style={{ marginTop: '4vh', padding: '1vw', paddingTop: 0 }}>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleCancel}
                    >
                        Cancel
                    </Button>
                </Grid>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleChangesSubmission}
                    >
                        Submit Changes
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default EditDetails;
