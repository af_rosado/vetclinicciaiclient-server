import React from 'react';
import {Grid, Typography, Link, TextField, Button} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { togglePasswordChange, changeUserPassword } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(() => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: 10
    }
}));


const ChangePassword = () => {
    const dispatch = useDispatch();
    const classes = useStyle();

    let userId = localStorage.getItem('id');
    const passwordChange = useSelector((state:any) => state.CurrentUserReducer.passwordChange);
    let newPassword = "";
    let confirm = "";

    const handleNew = (event: React.ChangeEvent<HTMLInputElement>) => {
        newPassword = event.target.value;
    }

    const handleConfirm = (event: React.ChangeEvent<HTMLInputElement>) => {
        confirm = event.target.value;
    }

    const handleChangesSubmission = () => {
        if ( newPassword !== confirm ){
            alert("Passwords don't match!");
        } else if (newPassword === ""){
            alert("You must insert a password");
        } else {
            let data: {} = {
                password: newPassword
            }
            dispatch(changeUserPassword(userId, data));
            dispatch(togglePasswordChange(!passwordChange));
        }
    }

    const handleCancel = () => {
        dispatch(togglePasswordChange(!passwordChange));
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            {
                <Typography variant="h5" style={{ marginTop: '2vh' }}>Change Password</Typography>
            }
            <Grid container spacing={2} style={{ marginTop: '2vh' }}>
                <Grid item xs={12} style={{textAlign: 'left', marginLeft: '2vw'}}>
                    <TextField
                        label={"New password"}
                        type='password'
                        onChange={handleNew}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} >
                <Grid item xs={12} style={{textAlign: 'left', marginLeft: '2vw'}}>
                    <TextField
                        label={"Confirm new password"}
                        type='password'
                        onChange={handleConfirm}
                    />
                </Grid>
            </Grid>

            <Grid container direction='row' style={{marginTop: '4vh', marginLeft: '2vw'}}>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleChangesSubmission}
                    >
                        Change Password
                    </Button>
                </Grid>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleCancel}
                    >
                        Cancel
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default ChangePassword;
