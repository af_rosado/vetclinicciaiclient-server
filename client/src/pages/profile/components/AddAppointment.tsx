import React, { useState, useEffect } from 'react';
import { Grid, Typography, Link, TextField, Button, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { updateUserDetails, toggleEdition, addPet, toggleAddPet, getAllShifts, getShiftsOnDay } from '../../../redux/actions/CurrentUserActions';
import Calendar from 'react-calendar';

const useStyle = makeStyles(() => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: '1vw'
    }
}));


const AddAppointment = () => {
    const classes = useStyle();
    const dispatch = useDispatch();

    const [date, setDate] = useState<Date | Date[]>(new Date())

    let userId = localStorage.getItem('id');
    const addApt = useSelector((state: any) => state.CurrentUserReducer.addApt);
    const shifts: [] = useSelector((state: any) => state.CurrentUserReducer.shifts);

    let selectedHour: [{ id: any, hour: any }] = [{ id: -1, hour: -1 }];

    useEffect(() => {
        dispatch(getShiftsOnDay(getFormatedDate()))
    }, [date])

    const handleChangesSubmission = () => {

    }

    const getFormatedDate = () => {
        let formated = "";
        if (date instanceof Date) {
            let day = date.getUTCDate();
            let month = date.getMonth() + 1;
            let year = date.getFullYear();

            if (day < 10) {
                formated = `${year}-${month}-0${day}T07:00:00`;
            } else if (month < 10) {
                formated = `${year}-0${month}-${day}T07:00:00`;
            } else {
                formated = `${year}-${month}-${day}T07:00:00`;
            }
        }
        return formated
    }

    const handleDateChange = (newDate: Date | Date[]) => {
        setDate(newDate);
    }

    const handleCancel = () => {
        dispatch(toggleAddPet(!addApt))
    }

    const handleBooking = (i: any) => {
        let s = i.length !== 0 ? new Date(i.startDate).getHours() : 0
        let ss = i.length !== 0 ? new Date(i.startDate).getMinutes() : 0
        let startHours = "";
        let startMinutes = ss < 10 ? `0${ss}` : ss;
        if (s < 10) {
            startHours = s >= 12 ? `0${s}:${startMinutes}` : ` 0${s}:${startMinutes}`;
        } else {
            startHours = s >= 12 ? `${s}:${startMinutes}` : ` ${s}:${startMinutes}`;
        }
        let e = i.length !== 0 ? new Date(i.endDate).getHours() : 0
        let ee = i.length !== 0 ? new Date(i.endDate).getMinutes() : 0
        let endHours = "";
        let endMinutes = ee < 10 ? `0${ee}` : ee;
        if (e < 10) {
            endHours = e >= 12 ? `0${e}:${endMinutes}` : ` 0${e}:${endMinutes}`;
        } else {
            endHours = e >= 12 ? `${e}:${endMinutes} ` : ` ${e}:${endMinutes}`;
        }
    }

    const handleHourPicking = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>, id: number) => {
        selectedHour.forEach((h, i) => {
            if (h.id === id) {
                selectedHour.splice(i, 1)
            }
        })
        selectedHour.push({
            id: id,
            hour: event.target.value,
        });
        console.log(selectedHour)
    }

    const getSchedule = () => {
        let list: [JSX.Element] = [<div key={Math.random() * 8}></div>];
        shifts.forEach(i => {
            list.push(getScheduleCard(i))
        });
        return list;
    }

    const getScheduleCard = (i: any) => {
        if (i.vet.frozen === true) return (<div key={Math.random() * 8}></div>);

        let { id } = i;

        let s = i.length !== 0 ? new Date(i.startDate).getHours() : 0
        let ss = i.length !== 0 ? new Date(i.startDate).getMinutes() : 0
        let startHours = "";
        let startMinutes = ss < 10 ? `0${ss}` : ss;
        if (s < 10) {
            startHours = s >= 12 ? `0${s}:${startMinutes} PM` : ` 0${s}:${startMinutes} AM`;
        } else {
            startHours = s >= 12 ? `${s}:${startMinutes} PM` : ` ${s}:${startMinutes} AM`;
        }

        let e = i.length !== 0 ? new Date(i.endDate).getHours() : 0
        let ee = i.length !== 0 ? new Date(i.endDate).getMinutes() : 0
        let endHours = "";
        let endMinutes = ee < 10 ? `0${ee}` : ee;
        if (e < 10) {
            endHours = e >= 12 ? `0${e}:${endMinutes} PM` : ` 0${e}:${endMinutes} AM`;
        } else {
            endHours = e >= 12 ? `${e}:${endMinutes} PM` : ` ${e}:${endMinutes} AM`;
        }

        return (
            <Grid item xs key={Math.random() * 8}>
                <Paper style={{ marginTop: '1vh' }}>
                    <Grid container justify='space-evenly'>
                        <Grid item xs={4}>
                            <Grid container alignItems='center' justify='space-between' style={{ border: 1, borderStyle: 'solid', padding: 5 }}>
                                <Grid item xs>
                                    <Typography style={{ fontWeight: 500 }}>Veterinary: </Typography>
                                </Grid>
                                <Grid item xs>
                                    <Typography>{i.vet.name}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={3}>
                            <Grid container alignItems='center' justify='space-between' style={{ border: 1, borderStyle: 'solid', padding: 5 }}>
                                <Grid item xs>
                                    <Typography style={{ fontWeight: 500 }}>From: </Typography>
                                </Grid>
                                <Grid item xs>
                                    <Typography>{startHours}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={3}>
                            <Grid container alignItems='center' justify='space-between' style={{ border: 1, borderStyle: 'solid', padding: 5 }}>
                                <Grid item xs>
                                    <Typography style={{ fontWeight: 500 }}>To: </Typography>
                                </Grid>
                                <Grid item xs>
                                    <Typography>{endHours}</Typography>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid container justify='space-evenly' alignItems='center' style={{ marginTop: '1vh' }}>
                        <Grid item xs={4} />
                        <Grid item xs={3}>
                            <Grid container alignItems='center' justify='space-between' style={{ border: 1, borderStyle: 'solid', padding: 5 }}>
                                <Grid item xs>
                                    <TextField
                                        label='Time'
                                        defaultValue="07:00"
                                        fullWidth
                                        type='time'
                                        onChange={(event) => handleHourPicking(event, id)}
                                    />
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item xs={3}>
                            <Button
                                style={{ color: colors.white, backgroundColor: colors.primary, }}
                                onClick={() => handleBooking(i)}
                                fullWidth
                            >
                                Book
                    </Button>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        );
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            {
                <Typography variant="h5" style={{ marginTop: '2vh' }}>Book Appointment</Typography>
            }
            <Grid container direction='column' justify='center' style={{ textAlign: 'center', marginTop: '2vh' }}>
                <Grid item xs={12} style={{ paddingLeft: '27.5%' }}>
                    <Calendar
                        value={date}
                        onChange={handleDateChange}
                    />
                </Grid>
                <Grid item xs={12} style={{ marginTop: '1vh' }}>
                    <Grid container>
                        <Grid item xs>
                            {
                                shifts.length !== 0
                                    ?
                                    getSchedule()
                                    :
                                    ""
                            }
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
            <Grid container justify='space-between' style={{ marginTop: '4vh', padding: '1vw', paddingTop: 0 }}>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleCancel}
                    >
                        Cancel
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default AddAppointment;
