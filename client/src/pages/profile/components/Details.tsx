import React, { useEffect } from 'react';
import { Grid, Typography, Link } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { useDispatch } from 'react-redux';
import { setPetProfile } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(() => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: 10
    }
}));

interface IProps {
    userType: string | null,
    user: User | Vet | Client,
    pets: [Pet],
    apts: [Appointment]
}

const Details = (props: IProps) => {
    const {
        userType,
        user,
        pets,
        apts
    } = props;
    
    const dispatch = useDispatch();

    const handlePetProfile = (p:Pet) => {
        dispatch(setPetProfile(p));
    }

    useEffect(() => {
    }, [user])

    const classes = useStyle();
    return (
        !user ? <div></div> :
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            {
                <Typography variant="h5" style={{ marginTop: '2vh' }}> {userType === 'VET'
                    ? "Veterinarian" : userType === 'CLIENT'
                        ? "Profile"
                        : "Admin"}
                </Typography>
            }
            <Grid container spacing={2} style={{ marginTop: '2vh' }} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Name:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <Typography>{user.name}</Typography>
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Email:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <Typography>{user.email}</Typography>
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between' >
                <Grid item xs={1} >
                    <Typography className={classes.category}>Phone:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <Typography>{user.phone}</Typography>
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Address:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <Typography>{user.address}</Typography>
                </Grid>
            </Grid>
            {
                userType !== 'CLIENT' ? "" :
                    <Grid container spacing={2} justify='space-between'>
                        <Grid item xs={1} >
                            <Typography className={classes.category}>Pets:</Typography>
                        </Grid>
                        <Grid item xs={10} style={{ textAlign: 'left' }}>
                            {
                                user.type !== 'CLIENT'
                                    ? "Not a Client"
                                    : pets[0] === emptyPet
                                        ? <Typography>You have no pets registered</Typography>
                                        : renderPetList(pets, handlePetProfile)
                            }
                        </Grid>
                    </Grid>
            }
            {
                userType === 'ADMIN' ? "" :
                    <Grid container spacing={2} justify='space-between'>
                        <Grid item xs={1} >
                            <Typography className={classes.category}>Appointments:</Typography>
                        </Grid>
                        <Grid item xs={10} style={{ textAlign: 'left' }}>
                            {
                                userType === 'ADMIN'
                                    ? "Is Admin"
                                    : apts[0] === emptyAppointment
                                        ? <Typography>You have no appointments</Typography>
                                        : renderAptList(apts)
                            }
                        </Grid>
                    </Grid>
            }
        </Grid>
    );
}

const renderPetList = (pets: [Pet], handlePetProfile: Function) => {
    let arr: JSX.Element[] = [];
    pets.map(p => {
        arr.push(
            <Grid key={Math.random() * 8} container justify='space-between'>
                <Grid item xs={4}>
                    <Typography >
                        {p.name}
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Link
                        style={{ color: colors.primary }}
                        onClick={() =>handlePetProfile(p)}
                    >
                        (Open Profile)
                    </Link>
                </Grid>
            </Grid>

        );
    });
    return arr;
}

const renderAptList = (apts: [Appointment]) => {
    let arr: JSX.Element[] = [];
    apts.map(a => {
        arr.push(
            <Grid key={Math.random() * 8} container justify='space-between'>
                <Grid item xs={4}>
                    <Typography >
                        {a.date}
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Link>
                        (Open Appointment)
                    </Link>
                </Grid>
            </Grid>

        );
    });
    return arr;
}

export default Details;
