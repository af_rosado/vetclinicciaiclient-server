import React from 'react';
import { Grid, Typography, Link, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useDispatch, useSelector } from 'react-redux';

import { User, Vet, Client, Pet, Appointment, emptyPet, emptyAppointment } from '../../../interfaces/Interfaces';
import { colors } from '../../../constants';
import { updateUserDetails, toggleEdition, addPet, toggleAddPet } from '../../../redux/actions/CurrentUserActions';

const useStyle = makeStyles(theme => ({
    blueBorder: {
        border: 2,
        borderStyle: 'solid',
        borderColor: colors.primary,
        textAlign: 'center',
        marginTop: '4vh',
        padding: 10,
        backgroundColor: 'white'
    },
    description: {
        marginLeft: '-25vw',
        marginTop: 0,
    },
    category: {
        fontWeight: 500,
        textAlign: 'left',
        paddingLeft: '1vw'
    }
}));


const AddPet = () => {
    const classes = useStyle();
    const dispatch = useDispatch();

    let userId = localStorage.getItem('id');
    const addPetB = useSelector((state: any) => state.CurrentUserReducer.addPet);
    let { name, species, age, description, status, picture } = emptyPet;

    const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        name = event.target.value;
    }

    const handleSpeciesChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        species = event.target.value;
    }

    const handleAgeChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        age = parseInt(event.target.value);
    }

    const handleDescriptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        description = event.target.value;
    }

    const handleStatusChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        status = event.target.value;
    }

    const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        picture = event.target.value;
    }

    const handleChangesSubmission = () => {
        if (name !== emptyPet.name
            && species !== emptyPet.species
            && age !== emptyPet.age
            && description !== emptyPet.description
            && status !== emptyPet.status
            && picture !== emptyPet.picture
        ) {
            let data: {} = {
                name: name,
                species: species,
                age: age,
                description: description,
                status: status,
                picture: picture
            }
            dispatch(addPet(userId, data));
            dispatch(toggleAddPet(!addPetB));
        } else {
            alert("You must complete all fields!")
        }
    }

    const handleCancel = () => {
        dispatch(toggleAddPet(!addPetB))
    }

    return (
        <Grid item xs className={`${classes.blueBorder} ${classes.description}`}>
            {
                <Typography variant="h5" style={{ marginTop: '2vh' }}>Add Pet</Typography>
            }
            <Grid container spacing={2} style={{ marginTop: '2vh' }} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Name:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={name}
                        onChange={handleNameChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Species:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={species}
                        onChange={handleSpeciesChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between' >
                <Grid item xs={1} >
                    <Typography className={classes.category}>Age:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={age.toString()}
                        onChange={handleAgeChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Description:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={description}
                        onChange={handleDescriptionChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Status:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={status}
                        onChange={handleStatusChange}
                    />
                </Grid>
            </Grid>
            <Grid container spacing={2} justify='space-between'>
                <Grid item xs={1} >
                    <Typography className={classes.category}>Picture:</Typography>
                </Grid>
                <Grid item xs={10} style={{ textAlign: 'left' }}>
                    <TextField
                        placeholder={picture}
                        onChange={handlePictureChange}
                    />
                </Grid>
            </Grid>
            <Grid container justify='space-between' style={{ marginTop: '4vh', padding: '1vw', paddingTop: 0 }}>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleCancel}
                    >
                        Cancel
                    </Button>
                </Grid>
                <Grid item xs={3} style={{ textAlign: 'center', padding: 5 }}>
                    <Button
                        fullWidth
                        style={{ color: colors.white, backgroundColor: colors.primary }}
                        onClick={handleChangesSubmission}
                    >
                        Add Pet
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default AddPet;
