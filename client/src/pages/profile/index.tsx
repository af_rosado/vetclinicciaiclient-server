import React, { useEffect } from 'react';
import { Container, Grid, Paper } from '@material-ui/core';
import CSS from 'csstype';
import { useDispatch, useSelector } from 'react-redux';

import { colors } from '../../constants';
import {
    getUserDetails,
    getUserPets,
    getUserApts,
    getVetDetails,
    getVetSchedule,
    getAdminDetails,
} from '../../redux/actions/CurrentUserActions';
import Details from './components/Details';
import EditDetails from './components/EditDetails';
import Commands from './components/Commands';
import ChangePassword from './components/ChangePassword';
import AddPet from './components/AddPet';
import RemovePet from './components/RemovePet';
import PetProfile from './components/PetProfile';
import { emptyPet } from '../../interfaces/Interfaces';

const imgStyle: CSS.Properties = {
    maxWidth: '260px',
    border: '2',
    borderStyle: 'solid',
    borderColor: colors.primary
};

const Profile = () => {
    const dispatch = useDispatch();

    // let userId = useSelector((state: any) => state.AuthReducer.id);
    let userId = localStorage.getItem('id');
    // let userType = useSelector((state: any) => state.AuthReducer.type);
    let userType = localStorage.getItem('type')

    const edition = useSelector((state: any) => state.CurrentUserReducer.edition);
    const passwordChange = useSelector((state: any) => state.CurrentUserReducer.passwordChange);
    const addPet = useSelector((state: any) => state.CurrentUserReducer.addPet);
    const remPet = useSelector((state: any) => state.CurrentUserReducer.removePet);
    const petP = useSelector((state: any) => state.CurrentUserReducer.petProfile);

    useEffect(() => {
        if (userType === 'CLIENT') {
            dispatch(getUserDetails(userId));
            dispatch(getUserPets(userId));
            dispatch(getUserApts(userId, 'clients'));
        } else if (userType === 'VET') {
            dispatch(getVetDetails(userId));
            dispatch(getUserApts(userId, 'vets'));
            dispatch(getVetSchedule(userId));
        } else if (userType === 'ADMIN') {
            dispatch(getAdminDetails(userId));
        } else console.log("USER TYPE IS WRONG")
    }, [edition, passwordChange, addPet, remPet]);

    let user = useSelector((state: any) => state.CurrentUserReducer.user);
    let pets = useSelector((state: any) => state.CurrentUserReducer.pets);
    let apts = useSelector((state: any) => state.CurrentUserReducer.appointments);
    let schedule = useSelector((state: any) => state.CurrentUserReducer.schedule);


    return (
        <Container>
            <Grid container>
                <Grid item xs>
                    <Grid container direction='column' >
                        <Paper style={{maxWidth: 260}}>
                        <Grid item xs={12}>
                            <img
                                src={user.picture === "" ? '/img/default-profile-picture.jpg' : user.picture}
                                alt="profile picture"
                                style={imgStyle}
                            />
                        </Grid>
                        <Commands userType={userType} />
                        </Paper>
                    </Grid>
                </Grid>
                {
                    edition
                        ?
                        <EditDetails
                            user={user}
                        />
                        :
                        passwordChange
                            ?
                            <ChangePassword />
                            :
                            addPet
                                ?
                                <AddPet />
                                :
                                remPet
                                    ?
                                    <RemovePet />
                                    :
                                    petP !== emptyPet
                                        ?
                                        <PetProfile pet={petP} />
                                        :
                                        <Details
                                            userType={userType}
                                            user={user}
                                            pets={pets}
                                            apts={apts}
                                        />
                }
            </Grid>
        </Container>
    );
};

// function isClient(client: User | Client | Vet): client is Client {
//     return 'pets' in client;
// }

// function isNotAdmin(admin: User | Client | Vet): admin is Client | Vet {
//     return ('pets' in admin || 'scheduleId' in admin);
// }

export default Profile;
