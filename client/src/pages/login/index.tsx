import React, { useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Grid, Card, CardContent, Tabs, Tab, Typography, Box } from '@material-ui/core';
import { Animated } from "react-animated-css";
import LoginForm from './components/LoginForm';
import RegisterForm from './components/RegisterForm';

const Login = () => {
   const classes = useStyles();
   const [tab, setTab] = useState(0);

   const handleTabChange = (event: React.ChangeEvent<{}>, newTab: number) => {
      setTab(newTab);
   };

   return (
      <Grid container direction='row' justify='center' alignItems='center' className={classes.Login}>

         <Grid item xs={6} >
            <Grid container direction='row' justify='center' alignItems='center' className={classes.Login}>
               <Animated animationIn="zoomInDown" animationOut="fadeOutRightBig" animationInDuration={1600} animationOutDuration={1400} isVisible={true}>
                  <img src="/img/darkPawSmall.png" />
               </Animated>
            </Grid>

         </Grid>
         <Grid item xs={6}>
            <Grid container direction='row' justify='flex-start' alignItems='center' className={classes.Login}>
               <Card>
                  <CardContent>
                     <Tabs
                        value={tab}
                        indicatorColor="primary"
                        textColor="primary"
                        onChange={handleTabChange}
                     >
                        <Tab label="Login" />
                        <Tab label="Register" />
                     </Tabs>
                     <TabPanel value={tab} index={0}>
                        <LoginForm/>
                     </TabPanel>
                     <TabPanel value={tab} index={1}>
                        <RegisterForm/>
                     </TabPanel>
                  </CardContent>
               </Card>

            </Grid>
         </Grid>
      </Grid>
   )
}

export default Login;

interface TabPanelProps {
   children?: React.ReactNode;
   index: any;
   value: any;
}

function TabPanel(props: TabPanelProps) {
   const { children, value, index, ...other } = props;

   return (
      <Typography
         component="div"
         role="tabpanel"
         hidden={value !== index}
         id={`simple-tabpanel-${index}`}
         aria-labelledby={`simple-tab-${index}`}
         {...other}
      >
         <Box p={3}>{children}</Box>
      </Typography>
   );
}


const useStyles = makeStyles(theme => ({
   Login: {
      height: '100vh',
      backgroundColor: '#000441',
   },
}))
