import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { TextField, Grid, Button, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { setLoginEmail, setLoginPassword, submitLogin } from '../../../redux/actions/LoginActions'

const LoginForm = () => {
   const classes = useStyles();
   const form = useSelector((state: any) => state.SessionReducer);
   const dispatch = useDispatch();

   const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setLoginEmail(event.target.value));
   };

   const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setLoginPassword(event.target.value));
   };

   const handleOnClick = () => {
      const data = {
         "email": form.email,
         "password": form.password
      };

      dispatch(submitLogin(data))
   };

   return (
      <Grid container direction='column' spacing={2}>
         <Grid item xs={12}>
            <TextField
               label="Email"
               value={form.email}
               onChange={handleEmailChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Password"
               type='password'
               value={form.password}
               onChange={handlePasswordChange}
               fullWidth
            />
         </Grid>
         {form.invalidLogin &&
            <Grid item xs={12} style={{ textAlign: 'center', marginTop: 10 }}>
               <Typography variant='body1' style={{ color: "#ff0000" }}>
                  Invalid credentials
                </Typography>
            </Grid>
         }
         <Grid item xs={12}>
            <Button
               variant="contained" color="primary"
               className={classes.Button} fullWidth
               onClick={handleOnClick}
            >
               Login
            </Button>
         </Grid>
      </Grid>
   )
}

export default LoginForm;


const useStyles = makeStyles(theme => ({
   Button: {
      marginTop: 16
   },
}))
