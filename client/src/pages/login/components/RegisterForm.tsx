import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { TextField, Grid, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { setRegisterPassword, setRegisterEmail, setRegisterName,
    setRegisterPhone, setRegisterAddress, setRegisterPicture, registClient } from '../../../redux/actions/RegisterActions'

const RegisterForm = () => {
   const classes = useStyles();
   const form = useSelector((state: any) => state.RegisterReducer);
   const dispatch = useDispatch();

   const handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterName(event.target.value));
   };

   const handleEmailChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterEmail(event.target.value));
   };

   const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterPassword(event.target.value));
   };

   const handlePhoneChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterPhone(event.target.value));
   };

   const handleAddressChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterAddress(event.target.value));
   };

   const handlePictureChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      dispatch(setRegisterPicture(event.target.value));
   };   
  

   return (
      <Grid container direction='column' spacing={2}>
         <Grid item xs={12}>
            <TextField
               label="Name"
               value={form.name}
               onChange={handleNameChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Email"
               value={form.email}
               onChange={handleEmailChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Password"
               type='password'
               value={form.password}
               onChange={handlePasswordChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Phone"
               value={form.phone}
               onChange={handlePhoneChange}
               fullWidth
            />
         </Grid>         
         <Grid item xs={12}>
            <TextField
               label="Address"
               value={form.address}
               onChange={handleAddressChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <TextField
               label="Picture (link)"
               value={form.picture}
               onChange={handlePictureChange}
               fullWidth
            />
         </Grid>
         <Grid item xs={12}>
            <Button 
               variant="contained" color="secondary" 
               className={classes.Button} fullWidth
               onClick={()=>dispatch(registClient(form))}
            >
               Register
            </Button>
         </Grid>
      </Grid>
   )
}

export default RegisterForm;


const useStyles = makeStyles(theme => ({
   Button: {
      marginTop: 16
   },
}))