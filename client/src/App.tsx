import React from 'react';
import './App.css';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import AuthRouter from './navigation/AuthRouter';
import {
   createMuiTheme,
   makeStyles,
   ThemeProvider,
} from '@material-ui/core/styles';
import Navbar from './commons/navbar/Navbar';
import { colors } from './constants';
import { SnackbarProvider } from 'notistack';

const App: React.FC = () => {

   const classes = useStyles();

   return (
      <div className={classes.App}>
         <Provider store={store}>
            <SnackbarProvider 
               anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'right',
               }} 
               maxSnack={3}
            >
               <ThemeProvider theme={theme}>
                  <Navbar />
                  <AuthRouter />
               </ThemeProvider>
            </SnackbarProvider>
         </Provider>
      </div>
   );
}

export default App;


const useStyles = makeStyles(theme => ({
   App: {
      //backgroundImage: "url('/img/gato.png')",
      backgroundRepeat: "repeat",
      backgroundPosition: "center",
      height: '100vh',
   },
   logoHeader: {
      textAlign: 'right',
      paddingRight: 20
   },
   titleHeader: {
      textAlign: 'left',
      paddingLeft: 20
   }
}))

const theme = createMuiTheme({
   palette: {
      primary: {
         main: '#000441',
         
      },
      secondary: {
         main: '#3adafe'
      },

   },
   overrides: {
      MuiSvgIcon: {
         root: {
            width: 48, 
            height: 48
         }
      },
      MuiCardContent: {
         root: {
            padding: 0,
            "&:last-child": {
               paddingBottom: 0
            }
         }
      },
      MuiContainer: {
         root: {
            marginTop: 50
         }
      },
      MuiPaper: {
         root: {
            padding: 20
         }
      },
      MuiLink: {
         root: {
            '&:hover': {
               color: colors.lightGrey
            }
         }
      }
   }
})
