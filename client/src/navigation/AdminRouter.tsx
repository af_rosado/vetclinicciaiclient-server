import React from 'react';
import { Router, Switch, Route } from 'react-router';
import { history } from "../redux/store";
import { ConnectedRouter } from 'connected-react-router';

import HireAdmin from '../pages/admin/components/HireAdmin';
import FireAdmin from '../pages/admin/components/FireAdmin';
import HireVet from '../pages/admin/components/HireVet';
import FireVet from '../pages/admin/components/FireVet';
import SetVetSchedule from '../pages/admin/components/SetVetSchedule';
import CheckVetAppts from '../pages/admin/components/CheckVetAppts';
// import AdminInfo from "../pages/admin/components/AdminInfo";

const AdminRouter = () => {
   return(
      <Router history={history}>
         <Switch>
            {/*<Route exact path='/admin/profile' component={AdminInfo}/>*/}
            <Route path='/admin/hireAdmin' component={HireAdmin}/>
            <Route path='/admin/fireAdmin' component={FireAdmin}/>
            <Route path='/admin/hireVet' component={HireVet}/>
            <Route path='/admin/fireVet' component={FireVet}/>
            <Route path='/admin/setVetSchedule' component={SetVetSchedule}/>
            <Route path='/admin/checkVetAppts' component={CheckVetAppts}/>
         </Switch>
      </Router>
   )
}


export default AdminRouter;
