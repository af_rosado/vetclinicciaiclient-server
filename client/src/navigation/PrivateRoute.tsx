import React from 'react';
import { Route, Redirect } from 'react-router';
var jwtDecode = require('jwt-decode');

interface IPrivateRoute {
   component: any,
   path: string,
   exact: any,
   lock: string
}

const PrivateRoute = ({ component, path, exact, lock, ...rest }: IPrivateRoute) => {
   let token = localStorage.getItem('token');

   if(token !== null){
      let decodedToken = jwtDecode(token);

      if(decodedToken.type === lock){
         return(
            <Route exact={exact} path={path} component={component} {...rest}/>
         )
      }else{
         return(
            <Redirect to='/'/>
         )
      }      
   }
   else {
      return(
         <Redirect to='/login'/>
      )
   }
}

export default PrivateRoute;
