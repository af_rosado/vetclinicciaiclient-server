import React from 'react';
import { Router, Switch, Route } from 'react-router';
import { history } from "../redux/store";
import PrivateRoute from './PrivateRoute';
import { ConnectedRouter } from 'connected-react-router';
import Profile from '../pages/profile';

import Home from '../pages/home';
import Login from '../pages/login';
import Admin from '../pages/admin';

const AuthRouter = () => {
   return(
      <ConnectedRouter history={history}>
         <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/login" component={Login}/>
            <PrivateRoute exact lock='ADMIN' path='/admin/:page' component={Admin}/>
            <Route exact path="/profile" component={Profile} />
         </Switch>
      </ConnectedRouter>
   )
}


export default AuthRouter;
