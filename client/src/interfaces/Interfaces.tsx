export interface User {
    name: string,
    email: string,
    phone: string,
    picture: string,
    address: string,
    type: string
}

export interface Client extends User {
    pets: [Pet],
    appointments: [Appointment]
}

export interface Vet extends User {
    appointments: [Appointment],
    scheduleId: number
}

export interface Pet {
    id: string,
    name: string,
    species: string,
    age: number,
    description: string,
    status: string,
    picture: string
}

export interface Appointment {
    date: string,
    description: string,
}

export interface Schedule {
    month: string,
    vetId: string
}

export const emptySchedule = {
    month: "",
    vetId: ""
}

export const emptyPet: Pet = {
    id: '',
    name: "",
    species: "",
    age: 0,
    description: "(Short pet description)",
    status: "(e.g. allergies)",
    picture: "(Link)"
}

export const emptyAppointment: Appointment = {
    date: "date",
    description: ""
}

export const emptyUser: User = {
    name: "",
    email: "",
    phone: "",
    picture: "",
    address: "",
    type: ""
}

export const emptyClient: Client = {
    name: "",
    email: "",
    phone: "",
    picture: "",
    address: "",
    type: "",
    pets: [emptyPet],
    appointments: [emptyAppointment]
}

export const emptyVet: Vet = {
    name: "",
    email: "",
    phone: "",
    picture: "",
    address: "",
    type: "",
    appointments: [emptyAppointment],
    scheduleId: -1
}